<?php

namespace App\Command;

use App\Service\ImportCliniqueCivService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:import-clinique-civ',
    description: 'Importation des cliniques de Côte d\'Ivoire',
)]
class ImportCliniqueCivCommand extends Command
{
    public function __construct(
        private ImportCliniqueCivService $importCliniqueCivService
    ){
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->importCliniqueCivService->import($io);


        $io->success('L\'importation des cliniques est terminée');

        return Command::SUCCESS;
    }
}

<?php

namespace App\Command;

use App\Service\ImportHospitalService;
use App\Service\ImportHospitalAbidjanService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:import-hospital',
    description: 'Add a short description for your command',
)]
class ImportHospitalAbidjanCommand extends Command
{
    public function __construct(
        private ImportHospitalService $importHospitalService
    ){
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->importHospitalService->importHospital($io);

        return Command::SUCCESS;
    }
}

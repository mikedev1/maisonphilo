<?php

namespace App\Command;

use App\Service\ImportHospitalService;
use App\Service\ImportHospitalCivService;
use App\Service\ImportHostpitalCivInterieur;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Input\InputArgument;
use App\Service\ImportHostpitalCivInterieurService;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:import-hospital-civ',
    description: 'Add a short description for your command',
)]
class importHospitalCivCommand extends Command
{

    public function __construct(
        private ImportHospitalCivService $importHospitalCivService
    ){
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->importHospitalCivService->ImportHospitalCiv($io);

        return Command::SUCCESS;
    }
}

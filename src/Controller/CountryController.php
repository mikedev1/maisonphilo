<?php

namespace App\Controller;

use App\Entity\Country;
use App\Entity\ServicePlatform;
use App\Form\CountryType;
use App\Repository\CommercesRepository;
use App\Repository\CountryRepository;
use App\Repository\PartnerRepository;
use App\Repository\HospitalRepository;
use App\Repository\ServicePlatformRepository;
use App\Repository\SiteInternetRepository;
use App\Repository\SocialCenterRepository;
use App\Repository\StateDeviceRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/country")
 * @Template
 */
class CountryController extends AbstractController
{
    /**
     * @Route("/{id}", name="app_country_index", methods={"GET", "POST"}, requirements={"code":"\d+"})
     */
    public function index(
        Country $country,
        HospitalRepository $hospitalRepository,
        PartnerRepository $partnerRepository,
        ServicePlatformRepository $servicePlatformRepository,
        SocialCenterRepository $socialCenterRepository,
        StateDeviceRepository $stateDeviceRepository,
        CommercesRepository $commercesRepository,
        SiteInternetRepository $siteInternetRepository
    ): Response {

        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
        ];
        $breadcrumbInactive = $country;


        $servicePlatforms = $servicePlatformRepository->findBy([
            "isValid" => true,
            "isNotWorking" => false
        ]);
        $socialCenters = $socialCenterRepository->findBy([
            'country' => $country
        ]);
        $hospitals = $hospitalRepository->findBy([
            'country' => $country,
        ]);
        $partners = $partnerRepository->findBy([
            'country' => $country,
        ]);
        $stateDevices = $stateDeviceRepository->findBy([
            'country' => $country,
        ]);
        $commerces = $commercesRepository->findBy([
            'country' => $country,
        ]);
        $siteInternets = $siteInternetRepository->findBy([
            'country' => $country,
        ]);

        return $this->render('country/show.html.twig', [
            'country' => $country,
            'hospitals' => $hospitals,
            'partners' => $partners,
            'servicePlateforms' => $servicePlatforms,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
            'socialCenters' => $socialCenters,
            'servicePlatforms' => $servicePlatforms,
            'stateDevices' => $stateDevices,
            'commerces' => $commerces,
            'siteInternets' => $siteInternets,
        ]);
    }
}

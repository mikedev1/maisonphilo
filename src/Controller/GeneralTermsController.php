<?php

namespace App\Controller;

use App\Repository\GeneralTermsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GeneralTermsController extends AbstractController
{
    private $generalTermsRepository;

    public function __construct(GeneralTermsRepository $generalTermsRepository)
    {
        $this->generalTermsRepository = $generalTermsRepository->findAll();
    }
    #[Route('/general/terms', name: 'app_general_terms')]
    public function index(): Response
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
        ];
        $breadcrumbInactive = 'Conditions générales';
        return $this->render('general_terms/index.html.twig', [
            'generalTerms' => $this->generalTermsRepository,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
        ]);
    }
}

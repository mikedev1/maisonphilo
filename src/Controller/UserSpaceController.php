<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class UserSpaceController extends AbstractController
{

    #[Route('/user/space', name: 'app_user_space')]
    public function index(): Response
    {

        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
        ];
        $breadcrumbInactive = 'Contact';

        $user = $this->getUser();
        return $this->render('user_space/index.html.twig', [
            'user' => $user,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive
        ]);
    }

    #[Route('/user/edit', name: 'app_user_edit')]
    public function edit(
        Request $request,
        EntityManagerInterface $manager
    ): Response {

        $user = $this->getUser();

        $form = $this->createForm(UserType::class, $user, [
            'action' => $this->generateUrl('app_user_edit'),

        ]);


        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $manager->persist($user);
            $manager->flush();

            $this->addFlash(
                'success',
                'flashMessage'
            );
            return $this->redirectToRoute("app_user_space");
        }
        return $this->render('user_space/edit.html.twig', [
            'form' => $form->createView(),
            'user' => $user
        ]);
    }

    #[Route('/user/delete/{id}', name: 'app_user_delete')]
    public function delete(UserRepository $userRepository): Response
    {
        $user = $this->getUser();
            $userRepository->remove($user, true);

        return $this->redirectToRoute('app_user_index', [], Response::HTTP_SEE_OTHER);
    }
}

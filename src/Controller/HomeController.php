<?php

namespace App\Controller;

use App\Entity\GlobalSearch;
use App\Form\GlobalSearchType;
use App\Repository\GlobalSearchRepository;
use App\Repository\ServicePlatformRepository;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Template
 * @Route("/")
 */
class HomeController extends AbstractController
{
    /**
     * @Route("/")
     */
    public function index(
        GlobalSearchRepository $globalSearchRepository,
        ServicePlatformRepository $servicePlatformRepository
    ) {

        /* Laptop */
        $informations = [
            [
                'content' => "Vous cherchez une information",
                'path' => 'app_assistance_share'
            ],
            [
                'content' => "Vous avez une information à partager",
                'path' => 'app_contact_index'
            ],
            [
                'content' => "Vous souhaitez découvrir les actions sociales",
                'path' => 'app_assistance_social'
            ],
        ];

        /* smartphone */
        $smartInformations = [
            [
                'content' => "Trouver une information",
                'path' => 'app_assistance_share',
                'font' => 'circle-info'
            ],
            [
                'content' => "Actions sociales",
                'path' => 'app_assistance_social',
                'font' => 'hand-holding-heart'
            ],
            [
                'content' => "Partager vos informations",
                'path' => 'app_contact_index',
                'font' => 'share'
            ],
        ];


        $globalSearch = new GlobalSearch();
        $form = $this->createForm(GlobalSearchType::class, $globalSearch);

        if ($form->isSubmitted() && $form->isValid()) {
            $redirect = $form->getData();
            return $this->redirect('app_contact_index');
        }
        $globalSearchs = $globalSearchRepository->findAll();
        $servicePlatforms = $servicePlatformRepository->findBy([
            'isValid' => true
        ]);
        return [
            'globalSearchs' => $globalSearchs,
            'form' => $form->createView(),
            'servicePlatforms' => $servicePlatforms,
            'informations' => $informations,
            'smartInformations' => $smartInformations
        ];
    }
 
}

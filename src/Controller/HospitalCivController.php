<?php

namespace App\Controller;

use App\Repository\CityRepository;
use App\Repository\CliniqueRepository;
use App\Repository\HospitalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HospitalCivController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */

    public function __construct(
        private HospitalRepository $hospitalRepository,
        private CliniqueRepository $cliniqueRepository,
        private CityRepository $cityRepository,
        private PaginatorInterface $paginator,
        private EntityManagerInterface $em,
    ) {
        $this->hospitalRepository = $hospitalRepository;
        $this->cliniqueRepository = $cliniqueRepository;
        $this->cityRepository = $cityRepository;
        $this->paginator = $paginator;
        $this->em = $em;
    }

    #[Route('/hospital/abidjan', name: 'app_hospital_abidjan')]
    public function abidjan(
        Request $request
    ): Response {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
            ['name' => 'Hôpitaux', 'path' => 'app_assistance_hospital'],
        ];
        $breadcrumbInactive = 'Hopitaux Abidjan';

        $dql = "SELECT h  FROM App\Entity\Hospital h 
                JOIN  App\Entity\Quartier q
                JOIN App\Entity\City c
                WHERE c.slug LIKE '%abi%' AND q.id = h.quartier
                ORDER BY q.name
                ";

        $query = $this->em->createQuery($dql);

        $pagination = $this->paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            20 /*limit per page*/
        );
        return $this->render('hospital/abidjan.html.twig', [
            'breadcrumbInactive' => $breadcrumbInactive,
            'breadcrumb' => $breadcrumb,
            'pagination' => $pagination
        ]);
    }

    #[Route('/hospital/interieur', name: 'app_hospital_interieur')]
    public function interieur(Request $request): Response
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
            ['name' => 'Hôpitaux', 'path' => 'app_assistance_hospital'],
        ];
        $breadcrumbInactive = 'Hôpitaux publics Côte d\'Ivoire';

        $dql = "SELECT h  FROM App\Entity\Hospital h 
        JOIN  App\Entity\City c
        WHERE c.slug <> 'abi' AND c.id = h.city
        ORDER BY c.name
        ";

        $query = $this->em->createQuery($dql);

        $pagination = $this->paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            50 /*limit per page*/
        );

        return $this->render('hospital/interieur.html.twig', [
            'breadcrumbInactive' => $breadcrumbInactive,
            'breadcrumb' => $breadcrumb,
            'pagination' => $pagination
        ]);
    }

    #[Route('/hospital/clinique', name: 'app_hospital_clinique')]
    public function clinique(
        Request $request
    ): Response {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
            ['name' => 'Hôpitaux', 'path' => 'app_assistance_hospital'],
        ];
        $breadcrumbInactive = 'Clinique Abidjan';

        $dql = "SELECT clinique  FROM App\Entity\Clinique clinique 
        JOIN  App\Entity\Quartier q
        JOIN App\Entity\City c
        WHERE c.slug LIKE '%abi%' AND q.id = clinique.quartier
        ORDER BY q.name
        ";

        $query = $this->em->createQuery($dql);

        $pagination = $this->paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            200 /*limit per page*/
        );

        return $this->render('hospital/clinique.html.twig', [
            'breadcrumbInactive' => $breadcrumbInactive,
            'breadcrumb' => $breadcrumb,
            'cliniques' => $pagination,
        ]);
    }

    #[Route('/hospital/cliniqueCiv', name: 'app_hospital_cliniqueCiv')]
    public function cliniqueCiv(
        Request $request,
    ): Response {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
            ['name' => 'Hôpitaux', 'path' => 'app_assistance_hospital'],
        ];
        $breadcrumbInactive = 'Clinique publics Côte d\'Ivoire';

        $dql = "SELECT clinique  FROM App\Entity\Clinique clinique 
        JOIN App\Entity\City c
        WHERE c.slug <> 'abi' AND c.id = clinique.city
        ORDER BY c.name
        ";

        $query = $this->em->createQuery($dql);

        $cliniques = $this->paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            100 /*limit per page*/
        );

        return $this->render('hospital/cliniqueCiv.html.twig', [
            'breadcrumbInactive' => $breadcrumbInactive,
            'breadcrumb' => $breadcrumb,
            'cliniques' => $cliniques,
        ]);
    }
}

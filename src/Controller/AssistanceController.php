<?php

namespace App\Controller;

use App\Repository\CommercesRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\CountryRepository;
use App\Repository\HospitalRepository;
use App\Repository\PartnerRepository;
use App\Repository\PharmacyRepository;
use App\Repository\ServicePlatformRepository;
use App\Repository\SiteInternetRepository;
use App\Repository\StateDeviceRepository;
use Knp\Component\Pager\PaginatorInterface; // Nous appelons le bundle KNP Paginator
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Template
 * @Route("/assistance")
 */
class AssistanceController extends AbstractController
{
    private $countryValid;
    private $servicePlatforms;
    private $countries;
    private $partners;
    private $paginator;
    private $monthPartners;
    private $stateDevices;
    private $commerces;
    private $siteInternet;
    private $pharmacies;

    public function __construct(
        CountryRepository $countryRepository,
        PartnerRepository $partnerRepository,
        ServicePlatformRepository $servicePlatformRepository,
        PaginatorInterface $paginator,
        StateDeviceRepository $stateDeviceRepository,
        CommercesRepository $commercesRepository,
        SiteInternetRepository $siteInternetRepository,
        PharmacyRepository $pharmacyRepository
    ) {
        $this->countryValid = $countryRepository
            ->findBy([
                "isValid" => true
            ]);
        $this->countries = $countryRepository->findAll();
        $this
            ->servicePlatforms = $servicePlatformRepository->findBy(
                [
                    "isValid" => true,
                ],
                [
                    "id" => "ASC"
                ]
            );
        $this->partners = $partnerRepository
            ->findBy(['isValid' => true]);
        $this->monthPartners = $partnerRepository->findBy([
            "isHonored" => true
        ]);
        $this->paginator = $paginator;
        $this->stateDevices = $stateDeviceRepository->findAll();
        $this->commerces = $commercesRepository->findAll();
        $this->siteInternet = $siteInternetRepository->findAll();
        $this->pharmacies = $pharmacyRepository->findAll();
    }
    /**
     * @Route("/")
     */
    public function share(): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index']
        ];
        $breadcrumbInactive = 'Partage d\'informations';

        return [
            'countries' => $this->countries,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
            'servicePlatforms' => $this->servicePlatforms
        ];
    }

    /**
     * @Route("/social")
     */
    public function social(): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
        ];
        $breadcrumbInactive = 'Social';
        return [
            'monthPartners' => $this->monthPartners,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
        ];
    }

    /* Information par thématique */

    /**
     * @Route("/hospital")
     */
    public function hospital(HospitalRepository $hospitalRepository): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
        ];
        $breadcrumbInactive = 'hôpital';

        $hospitals =  $hospitalRepository->findBy([
            'isGeriatric' => true
        ]);
        return [
            'countries' => $this->countries,
            'hospitals' => $hospitals,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
        ];
    }

    /**
     * @Route("/partner")
     */
    public function partner(Request $request): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
        ];
        $breadcrumbInactive = 'Association';

        $paginations = $this->paginator->paginate(
            $this->partners, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            5 // Nombre de résultats par page
        );

        return [
            'countries' => $this->countries,
            'partners' => $this->partners,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
            'paginations' => $paginations
        ];
    }
    /**
     * Dispositifs étatiques
     *
     * @return array
     * @Route("/stateDevice")
     */
    public function stateDevice(): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
        ];
        $breadcrumbInactive = 'Dispositifs étatiques';

        return [
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
            'stateDevices' => $this->stateDevices,
            'countries' => $this->countryValid
        ];
    }

    /**
     *
     * @Route("/socialCenter")
     */
    public function socialCenter(): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
        ];
        $breadcrumbInactive = 'Centres sociaux';

        return [
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive
        ];
    }


    /**
     *
     * @Route("/commerces")
     */
    public function commerces(): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
        ];
        $breadcrumbInactive = 'Commerces';

        return [
            'commerces' => $this->commerces,
            'countries' => $this->countries,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive
        ];
    }

    /**
     *
     * @Route("/siteInternet")
     */
    public function siteInternet(): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
        ];
        $breadcrumbInactive = 'Sites';

        return [
            'countries' => $this->countries,
            'siteInternet' => $this->siteInternet,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
            'siteInternets' => $this->siteInternet,
        ];
    }

    /**
     *
     * @Route("/pharmacy")
     */
    public function pharmacy(): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Partage d\'informations', 'path' => 'app_assistance_share'],
        ];
        $breadcrumbInactive = 'Pharmacie';

        return [
            'countries' => $this->countryValid,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
            'pharmacies' => $this->pharmacies,
        ];
    }
}

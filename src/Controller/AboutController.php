<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Template
 * @Route("/about")
 */
class AboutController extends AbstractController
{

    /**
     * @Route("/who/history")
     */
    public function history(): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index']
        ];
        $breadcrumbInactive = 'Histoire';
        return [
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
        ];
    }

    /**
     * @Route("/africa ")
     */
    public function africa(): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
        ];
        $breadcrumbInactive = 'Afrique';
        return [
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
        ];
    }

    /**
     * @Route("/world")
     */
    public function world(): array
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Afrique', 'path' => 'app_about_africa'],

        ];
        $breadcrumbInactive = 'Monde';
        return [
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
        ];
    }
}

<?php

namespace App\Controller;

use App\Entity\Survey;
use App\Form\SurveyType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SurveyController extends AbstractController
{
    #[Route('/survey', name: 'app_survey')]
    public function index(
        Request $request,
        EntityManagerInterface $entityManager
    ): Response {
        $survey = new Survey();
        $survey->setCreatedAt(new \DateTimeImmutable('now'));

        $form = $this->createForm(SurveyType::class, $survey);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $survey = $form->getData();

            $survey->setName($survey->getName());
            $survey->setLastName($survey->getLastName());
            $survey->setEmail($survey->getEmail());
            $survey->setCountry($survey->getCountry());
            $survey->setClothes($survey->getClothes());
            $survey->setName($survey->getName());
            $survey->setName($survey->getName());
            $survey->setName($survey->getName());
            $survey->setName($survey->getName());
            $survey->setName($survey->getName());

            $survey->setClothes(
                $form["clothes"]["__name__"]->getData()
            );

            $survey->setAids(
                $form["aids"]["__name__"]->getData()
            );

            $survey->setHospitals(
                $form["hospitals"]["__name__"]->getData()
            );

            $entityManager->persist($survey);
            $entityManager->flush();


            $this->addFlash(
                'notice',
                'Votre formulaire a bien été enregistré'
            );

            return $this->redirectToRoute('app_contact_index');
        }

        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
            ['name' => 'Contact', 'path' => 'app_contact_index']
        ];
        $breadcrumbInactive = 'Questionnaire';

        return $this->renderForm('survey/index.html.twig', [
            'form' => $form,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
        ]);
    }
}

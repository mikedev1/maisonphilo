<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class LoginController extends AbstractController
{
    #[Route('/login', name: 'app_login')]
    public function index(AuthenticationUtils $authenticationUtils): Response
    {
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();

        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if ($error) {
            $this->addFlash(
                'error',
                "L'association email / mot de passe est incorrecte. Veuillez renouveler votre saisie"
            );
        }

        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
        ];
        $breadcrumbInactive = 'Connexion';

        return $this->render('login/index.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error,
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive
        ]);
    }

    /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    #[Route('/logout', name: 'app_logout', methods:"GET")]
    public function logout(): void
    {
        //$this->addFlash('warning', 'Vous avez bien été déconnecté');
        // controller can be blank: it will never be called!
        throw new \Exception(
            'Don\'t forget to activate logout in security.yaml'
        );
    }
}

<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GetStartedController extends AbstractController
{
    #[Route('/get/started', name: 'app_get_started')]
    public function index(): Response
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
        ];
        $breadcrumbInactive = 'Comment ça marche?';

        return $this->render('get_started/index.html.twig', [
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
        ]);
    }
}

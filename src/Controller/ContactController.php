<?php

namespace App\Controller;

use DateTimeImmutable;
use App\Entity\Contact;
use App\Form\ContactType;
use App\Service\MailerService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Template
 * @Route("/contact")
 */
class ContactController extends AbstractController
{
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return Response
     * @Route("/")
     */
    public function index(
        Request $request,
        EntityManagerInterface $entityManager,
        MailerService $mailer
    ): Response {

        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
        ];
        $breadcrumbInactive = 'Contact';

        $contact = new Contact();

        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        /* soumission du formulaire */
        if ($form->isSubmitted() && $form->isValid()) {
            $contact = $form->getData();
            $contact->setcreatedAt(new DateTimeImmutable('now'));
            $entityManager->persist($contact);
            $entityManager->flush();
            $this->addFlash(
                'success',
                $contact->getEmail() . ', votre message a bien été envoyé!'
            );
            $mailer->sendContactEmail(
                $contact->getEmail(),
                $contact->getSubject(),
                $contact->getLastName(),
                $contact->getFirstName(),
                $contact->getCountry(),
                $contact->getContent()
            );
            //Documentation turbo drive un formuaire correctement soumis doit avoir une reponse 303 cad HTTP_SEE_OTHER
            return $this->redirectToRoute('app_contact_index', ['_fragment' => 'contact'], Response::HTTP_SEE_OTHER);
        }

        /* Formulaire soumis mais invalide */
        if ($form->isSubmitted() && !$form->isValid()) {
            $response = new Response();
            // $content = $this->renderView('contact/index.html.twig', [
            //  '_fragment' => 'contact',
            // 'form' => $form->createView(),
            // 'breadcrumb' => $breadcrumb,
            // 'breadcrumbInactive' => $breadcrumbInactive,
            // ]);
            // $response->setContent($content);
            $response->setStatusCode(Response::HTTP_UNPROCESSABLE_ENTITY);

            $this->addFlash(
                'error',
                'Votre message n\'a pas été envoyé, veillez renouveler votre saisie'
            );

            return $this->render('contact/index.html.twig', [
                '_fragment' => 'contact',
                'form' => $form->createView(),
                'breadcrumb' => $breadcrumb,
                'breadcrumbInactive' => $breadcrumbInactive,
            ], $response);
        }


        return $this->render('contact/index.html.twig', [
            'form' => $form->createView(),
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive,
        ]);
    }

    /**
     * @Route("/senior")
     */
    public function senior()
    {
        $breadcrumb = [
            ['name' => 'Accueil', 'path' => 'app_home_index'],
        ];
        $breadcrumbInactive = 'Seniors';
        return $this->render('contact/senior.html.twig', [
            'breadcrumb' => $breadcrumb,
            'breadcrumbInactive' => $breadcrumbInactive
        ]);
    }
}

<?php

namespace App\Controller;

use App\Repository\CountryRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SitemapController extends AbstractController
{
    #[Route('/sitemap.xml', name: 'app_sitemap', defaults:["_format" => "xml"])]
    public function index(
        Request $request,
        CountryRepository $countryRepository
    ): Response {
        /* Voir la documentation sur le site "https://www.sitemaps.org/fr/" */
        $hostname = $request->getSchemeAndHttpHost();
        /* Permet de récuperer "https://127.0.0.1:8000" */
        $urls = [];
        $urls[] = ['loc' => $this->generateUrl('app_home_index')];
        $urls[] = ['loc' => $this->generateUrl('app_about_who')];
        $urls[] = ['loc' => $this->generateUrl('app_about_history')];
        $urls[] = ['loc' => $this->generateUrl('app_about_objective')];
        $urls[] = ['loc' => $this->generateUrl('app_about_africa')];
        $urls[] = ['loc' => $this->generateUrl('app_about_world')];
        $urls[] = ['loc' => $this->generateUrl('app_assistance_share')];
        $urls[] = ['loc' => $this->generateUrl('app_assistance_social')];
        $urls[] = ['loc' => $this->generateUrl('app_assistance_hospital')];
        $urls[] = ['loc' => $this->generateUrl('app_assistance_partner')];
        $urls[] = ['loc' => $this->generateUrl('app_contact_index')];
        $urls[] = ['loc' => $this->generateUrl('app_register')];
        $urls[] = ['loc' => $this->generateUrl('app_survey')];

        foreach ($countryRepository->findAll() as $country) {
            $urls[] = [
                'loc' => $this->generateUrl('app_country_index', [
                    'id' => $country->getId(),
                ]),
            ];
        }

        /* preparation de la reponse */
        $response = new Response(
            $this->renderView('sitemap/index.html.twig', [
                'urls'      => $urls,
                'hostname'  => $hostname
            ]),
            200
        );
        /* Création du header de la reponse qu on aurait egalement pu creer dans lannotation du controller */
        $response->headers->set('Content-type', 'text/xml');

        return $response;
    }
}

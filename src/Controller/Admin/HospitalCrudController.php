<?php

namespace App\Controller\Admin;

use App\Entity\Hospital;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use phpDocumentor\Reflection\Types\Boolean;

class HospitalCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Hospital::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('servicePlatform', 'Catégorie'),
            AssociationField::new('city', 'Ville'),
            BooleanField::new('isGeriatric', 'Hôpital avec un service de gériatrie?'),
            TextField::new('name', 'Nom de l\'hopital'),
            TextField::new('subTitle', 'Sous titre'),
            TextEditorField::new('description', 'Description'),
            TextField::new('phoneNumber', 'Numéro de téléphone'),
            EmailField::new('email', 'Adresse mail'),
        ];
    }
}

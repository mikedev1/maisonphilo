<?php

namespace App\Controller\Admin;

use App\Entity\City;
use App\Entity\User;
use App\Entity\Survey;
use App\Entity\Contact;
use App\Entity\Country;
use App\Entity\Partner;
use App\Entity\Clinique;
use App\Entity\Hospital;
use App\Entity\Pharmacy;
use App\Entity\Commerces;
use App\Entity\StateDevice;
use App\Entity\GeneralTerms;
use App\Entity\GlobalSearch;
use App\Entity\SiteInternet;
use App\Entity\SocialCenter;
use App\Entity\HospitalAbidjan;
use App\Entity\ServicePlatform;
use App\Entity\Route as EntityRoute;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;

class DashboardController extends AbstractDashboardController
{
    #[Route('/admin', name: 'admin')]
    public function index(): Response
    {
        //return parent::index();

        // Option 1. You can make your dashboard redirect to some common page of your backend
        //
        // $adminUrlGenerator = $this->container->get(AdminUrlGenerator::class);
        // return $this->redirect($adminUrlGenerator->setController(OneOfYourCrudController::class)->generateUrl());

        // Option 2. You can make your dashboard redirect to different pages depending on the user
        //
        // if ('jane' === $this->getUser()->getUsername()) {
        //     return $this->redirect('...');
        // }

        // Option 3. You can render some custom template to display a proper dashboard with widgets, etc.
        // (tip: it's easier if your template extends from @EasyAdmin/page/content.html.twig)
        //
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            // the name visible to end users
            ->setTitle('Maison Philo')
            // you can include HTML contents too (e.g. to link to an image)
            ->setTitle(
                '<img src="images/logo_1.png" class="img-fluid w-25">
                 Maison Philo 
                 <span class="text-small">Admin.</span>'
            )

            // by default EasyAdmin displays a black square as its default favicon;
            // use this method to display a custom favicon: the given path is passed
            // "as is" to the Twig asset() function:
            // <link rel="shortcut icon" href="{{ asset('...') }}">
            ->setFaviconPath('svg/logo.svg')

            // the domain used by default is 'messages'

            // there's no need to define the "text direction" explicitly because
            // its default value is inferred dynamically from the user locale
            ->setTextDirection('ltr')

            // set this option if you prefer the page content to span the entire
            // browser width, instead of the default design which sets a max width
            ->renderContentMaximized()

            // set this option if you prefer the sidebar (which contains the main menu)
            // to be displayed as a narrow column instead of the default expanded design
            ->renderSidebarMinimized()

            // by default, users can select between a "light" and "dark" mode for the
            // backend interface. Call this method if you prefer to disable the "dark"
            // mode for any reason (e.g. if your interface customizations are not ready for it)
            // ->disableDarkMode()

            // by default, all backend URLs are generated as absolute URLs. If you
            // need to generate relative URLs instead, call this method
            ->generateRelativeUrls()

            // set this option if you want to enable locale switching in dashboard.
            // IMPORTANT: this feature won't work unless you add the {_locale}
            // parameter in the admin dashboard URL (e.g. '/admin/{_locale}').
            // the name of each locale will be rendered in that locale
            // (in the following example you'll see: "English", "Polski")

            // to further customize the locale option, pass an instance of
            // EasyCorp\Bundle\EasyAdminBundle\Config\Locale
            ->setLocales([
                'fr', // locale without custom options
            ]);
        }

        public function configureMenuItems(): iterable
        {
        yield MenuItem::linkToCrud(
            'Services de la platforme',
            'fa-solid fa-boxes-stacked',
            ServicePlatform::class
        );
        yield MenuItem::linkToCrud(
            'Routes du site',
            'fa-solid fa-road',
            EntityRoute::class
        );
        yield MenuItem::linkToCrud(
            'Formulaire de recherche',
            'fa fa-search',
            GlobalSearch::class
        );
        yield MenuItem::linkToCrud('Utilisateurs', 'fas fa-user', User::class);
        yield MenuItem::linkToCrud('Pays', 'fa fa-globe', Country::class);
        yield MenuItem::linkToCrud('Ville', 'fa fa-globe', City::class);

        yield MenuItem::linkToCrud(
            'Messages reçus',
            'fa-solid fa-envelope-open-text',
            Contact::class
        );
        yield MenuItem::linkToCrud(
            'Association',
            'fa-solid fa-hand-holding-heart',
            Partner::class
        );
        yield MenuItem::linkToCrud(
            'Hopital',
            'fa-solid fa-hospital',
            Hospital::class
        );

        yield MenuItem::linkToCrud(
            'Clinique',
            'fa-solid fa-house-medical', /* To do font for this */
            Clinique::class
        );
        yield MenuItem::linkToCrud(
            'Formulaires remplis',
            'fa-solid fa-square-poll-horizontal',
            Survey::class
        );
        yield MenuItem::linkToCrud(
            'centres sociaux',
            'fa-solid fa-person-shelter',
            SocialCenter::class
        );

        yield MenuItem::linkToCrud(
            'Dispositifs étatiques',
            'fa-solid fa-globe',
            StateDevice::class
        );

        yield MenuItem::linkToCrud(
            'Conditions générales',
            'fa-solid fa-file-signature',
            GeneralTerms::class
        );

        yield MenuItem::linkToCrud(
            'Commerces dédiés',
            'fa-solid fa-shop',
            Commerces::class
        );

        yield MenuItem::linkToCrud(
            'Sites internet',
            'fa-brands fa-internet-explorer', /* To do font for this */
            SiteInternet::class
        );

        yield MenuItem::linkToCrud(
            'Pharmacies',
            'fa-solid fa-house-medical-circle-check', /* To do font for this */
            Pharmacy::class
        );

    }
}

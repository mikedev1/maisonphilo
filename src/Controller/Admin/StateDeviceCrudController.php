<?php

namespace App\Controller\Admin;

use App\Entity\StateDevice;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class StateDeviceCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return StateDevice::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('country'),
            AssociationField::new('servicePlatform')->setLabel('Catégorie'),
            ArrayField::new('links'),
            TextField::new('name'),
            TextEditorField::new('description')
        ];
    }
}

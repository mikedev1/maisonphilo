<?php

namespace App\Controller\Admin;

use App\Entity\SocialCenter;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class SocialCenterCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SocialCenter::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            AssociationField::new('country')->setLabel('Pays'),
            AssociationField::new('servicePlatform')->setLabel('Catégorie'),
        ];
    }
}

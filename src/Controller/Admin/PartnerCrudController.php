<?php

namespace App\Controller\Admin;

use App\Entity\Partner;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ArrayField;
use EasyCorp\Bundle\EasyAdminBundle\Field\EmailField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PartnerCrudController extends AbstractCrudController
{

    public static function getEntityFqcn(): string
    {
        return Partner::class;
    }
    /**
     * Undocumented function
     *
     * @param Crud $crud
     * @return Crud
     */
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            // the visible title at the top of the page and the content of the <title> element
            // it can include these placeholders:
            //   %entity_name%, %entity_as_string%,
            //   %entity_id%, %entity_short_id%
            //   %entity_label_singular%, %entity_label_plural%
            ->setPageTitle('index', 'Liste des associations')

            // you can pass a PHP closure as the value of the title
            ->setPageTitle('new', 'Création association')

            // in DETAIL and EDIT pages, the closure receives the current entity
            // as the first argument
            ->setPageTitle('detail', fn (Partner $product) => (string) $product)
            ->setPageTitle('edit', fn (Partner $category) => sprintf('Modification <b>%s</b>', $category->getName()))

            // the help message displayed to end users (it can contain HTML tags)
            ->setHelp('edit', '...');
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('country')->autocomplete()->setLabel('Pays'),
            AssociationField::new('servicePlatform')->autocomplete()->setLabel('Catégorie'),
            TextField::new('name')->setLabel('Nom de l\'assocaition'),
            TextEditorField::new('address')->setLabel('Adresse')->onlyWhenCreating()->onlyWhenUpdating(),
            TextEditorField::new('description')->setLabel('Description')->onlyWhenCreating()->onlyWhenUpdating(),
            TextEditorField::new('activity')->setLabel('Activités')->onlyWhenCreating()->onlyWhenUpdating(),
            TextField::new('imageFile')->setFormType(VichImageType::class)->onlyWhenCreating()->onlyWhenUpdating(),
            ImageField::new('imageName')
                ->setBasePath('/uploads/partners/')
                ->setUploadDir('public/uploads/partners/')
                ->onlyOnIndex(),
            ArrayField::new('type')->setLabel('Type')->onlyWhenCreating()->onlyWhenUpdating(),
            TextField::new('contact')->setLabel('Président(e)')->onlyWhenCreating()->onlyWhenUpdating(),
            TextEditorField::new('phoneNumber')->setLabel('Téléphone')->onlyWhenCreating()->onlyWhenUpdating(),
            EmailField::new('email')->setLabel('Adresse mail')->onlyWhenCreating()->onlyWhenUpdating(),
            BooleanField::new('isHonored')->setLabel('Association mise à l\'honneur?'),
            BooleanField::new('isValid')->setLabel('Afficher sur le site?')
        ];
    }
}

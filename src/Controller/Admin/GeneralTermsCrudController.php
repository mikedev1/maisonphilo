<?php

namespace App\Controller\Admin;

use App\Entity\GeneralTerms;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class GeneralTermsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return GeneralTerms::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextEditorField::new('legalNotice'),
            TextEditorField::new('cookiePolicy'),
            TextEditorField::new('protectionOfPrivacy'),

        ];
    }
    public function configureActions(Actions $actions): Actions
    {
        return $actions
            // ...
            ->remove(Crud::PAGE_INDEX, Action::NEW)
        ;
    }
}

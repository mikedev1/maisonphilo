<?php

namespace App\Controller\Admin;

use App\Entity\Commerces;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class CommercesCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Commerces::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            AssociationField::new('country'),
            AssociationField::new('servicePlatform'),
            AssociationField::new('category'),
            TextEditorField::new('description'),
        ];
    }
}

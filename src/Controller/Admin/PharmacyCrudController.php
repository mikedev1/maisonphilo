<?php

namespace App\Controller\Admin;

use App\Entity\Pharmacy;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;

class PharmacyCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Pharmacy::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            AssociationField::new('country'),
            AssociationField::new('servicePlatform'),
            ImageField::new('imageFile'),
            TextEditorField::new('content'),
        ];
    }
   
}

<?php

namespace App\Controller\Admin;

use App\Entity\GlobalSearch;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class GlobalSearchCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return GlobalSearch::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('link'),
            AssociationField::new('path')->setCrudController(
                RouteCrudController::class
            ),
        ];
    }
}

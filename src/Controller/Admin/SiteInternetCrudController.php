<?php

namespace App\Controller\Admin;

use App\Entity\SiteInternet;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class SiteInternetCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return SiteInternet::class;
    }


    public function configureFields(string $pageName): iterable
    {
        return [
            AssociationField::new('country'),
            AssociationField::new('servicePlatform')->setLabel('Catégorie'),
            TextEditorField::new('description')
        ];
    }
}

<?php

namespace App\Controller\Admin;

use App\Entity\Clinique;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;

class CliniqueCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Clinique::class;
    }

    
    public function configureFields(string $pageName): iterable
    {
        return [
            TextField::new('name'),
            TextField::new('phoneNumber'),
            TextEditorField::new('information'),
            AssociationField::new('city'),
            AssociationField::new('country'),
            TextField::new('quartier'),
        ];
    }
   
}

<?php

namespace App\Controller\Admin;

use App\Entity\HospitalAbidjan;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class HospitalAbidjanCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return HospitalAbidjan::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}

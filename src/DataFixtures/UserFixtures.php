<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class UserFixtures extends Fixture
{
    private $passwordHasher;
    public function __construct(
        UserPasswordHasherInterface $passwordHasher
    ) {
        $this->passwordHasher = $passwordHasher;
    }
    public function load(
        ObjectManager $manager
    ) {
        $plaintextPassword = "aaaaaa";
        for ($i = 0; $i < 20; $i++) {
            # code..
            $user = new User();
            $hashedPassword = $this->passwordHasher->hashPassword(
                $user,
                $plaintextPassword
            );

            $user->setEmail($i . 'test@gmail.com');
            $user->setRoles(['ROLE_USER']);
            $user->setPassword($hashedPassword);
            $manager->persist($user);
        }
        $user = new User();
        $hashedPassword = $this->passwordHasher->hashPassword(
            $user,
            $plaintextPassword
        );
        $user->setEmail('servam95@gmail.com');
        $user->setRoles(['ROLE_ADMIN']);
        $user->setPassword($hashedPassword);
        $manager->persist($user);


        $manager->flush();
    }
}

<?php

namespace App\DataFixtures;

use App\Entity\Country;
use App\Entity\Partner;
use App\Entity\Hospital;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use SebastianBergmann\Type\TrueType;
use Symfony\Component\Validator\Constraints\Length;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        /* initialisation des données */
        $dataCountries = [
            "Côte d'Ivoire",
            "Togo",
            "Sénégal",
            "Cameroun",
            "Mali"
        ];
        /* Données concernant les partenaires */

        $dataPartners = [
            [
                "name" => "L'àge d'or",
                "address" => " xxx ",
                "description" => "Fondée en 2015, cette ONG à pour objectif la
                 sensibilisation des populations sur l'existence des personnes
                  agées afin de les valoriser et leur permettre de connaître
                   l'épanouissement.",
                "type" => ['ONG'],
                "activity" => "Actions sociales",
                "contact" => "Mme Apata",
                "phoneNumber" => "+225 XXXXXXX",
                "email" => "xxxx@mail.com",
                "isHonored" => false,
                "isValid" => false
            ],
            [
                "name" => "Association test",
                "address" => "Yamoussoukro",
                "description" => "Fondée en 2022, cette ONG a pour objectif la
                 sensibilisation des populations sur l'existence des personnes
                  agées afin de les valoriser et leur permettre de connaître
                   l'épanouissement.",
                "type" => ['ONG'],
                "activity" => "Actions sociales",
                "contact" => "Mme Serva",
                "phoneNumber" => "+225 XXXXXXX",
                "email" => "associationTest@mail.com",
                "isHonored" => true,
                "isValid" => true

            ],
        ];

        $dataHospitals = [
            [
                "name" => "Centre hospitalier et Universitaire d'Angré",
                "subTitle" => "Hôpital à Cocody, Abidjan",
                "content" => "Vous y trouverez un service gériatrie",
                "phoneNumber" => "+225 2722496400",
                "email" => "info@chuangre.ci"
            ],
            [
                "name" => "Centre médical LIRING",
                "subTitle" => "Remblais BVD Cameroun Résidence LES ARCADES Koumassi, Abidjan",
                "content" => "Vous y trouverez des médecins spécialisés en gériatrie",
                "phoneNumber" => "+225 21288402",
                "email" => ""
            ],
            [
                "name" => "PISAM",
                "subTitle" => "Cocody corniche, Abidjan",
                "content" => "Vous y trouverez des médecins spécialisés en prise en charge gériatrique",
                "phoneNumber" => "+225 22483130",
                "email" => "",
            ],
            [
                "name" => "Clinique médicale FARAH",
                "subTitle" => "Marcory résidentiel près de Loko Gd bloc, Abidjan",
                "content" => "Vous y trouverez des médecins spécialisés en prise en charge gériatrique",
                "phoneNumber" => "+225 21230800 // +225 47111111",
                "email" => "info@chuangre.ci",
            ],
            [
                "name" => "Polyclinique de l'INDENIE",
                "subTitle" => "Boulevard de l'INDENIE plateau, Abidjan",
                "content" => "Vous y trouverez des médecins spécialisés en prise en charge gériatrique",
                "phoneNumber" => "+225 20 30 91 00",
                "email" => "",
            ]
        ];

        /* add country */
        foreach ($dataCountries as $dataCountry) {
            if ($dataCountry == "Côte d'Ivoire") {
                $country = new Country();
                $country
                    ->setName($dataCountry)
                    ->setIsValid(true);
                $manager->persist($country);
            } else {
                $country = new Country();
                $country
                    ->setName($dataCountry)
                    ->setIsValid(false);
                ;
                $manager->persist($country);
            }
            /* Association avec la côte d'Ivoire */
            if ($dataCountry == $dataCountries["0"]) {
                /* Ajout des partenaires */
                foreach ($dataPartners as $dataPartner) {
                    $partner = new Partner();
                    $partner
                        ->setName($dataPartner["name"])
                        ->setAddress($dataPartner["address"])
                        ->setDescription($dataPartner["description"])
/*                         ->setType($dataPartner["type"])
 */                        ->setActivity($dataPartner["activity"])
                        ->setContact($dataPartner["contact"])
                        ->setPhoneNumber($dataPartner["phoneNumber"])
                        ->setEmail($dataPartner["email"])
                        ->setIsValid($dataPartner["isValid"])
                        ->setIsHonored($dataPartner["isHonored"])
                        ->setCountry($country);
                    $manager->persist($partner);
                }
                /* Ajout des hopitaux */
                foreach ($dataHospitals as $dataHospital) {
                    $hospital = new Hospital();
                    $hospital
                        ->setName($dataHospital["name"])
                        ->setSubTitle($dataHospital["subTitle"])
                        ->setContent($dataHospital["content"])
                        ->setPhoneNumber($dataHospital["phoneNumber"])
                        ->setEmail($dataHospital["email"])
                        ->setIsValid(true)
                        ->setCountry($country);
                    $manager->persist($hospital);
                }
            }
        }
        $manager->flush();
    }
}

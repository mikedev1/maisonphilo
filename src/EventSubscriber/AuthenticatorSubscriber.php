<?php

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Event\AuthenticationSuccessEvent;

class AuthenticatorSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents(): array
    {
        return [
            'security.authentication.success' =>
                'onSecurityAuthenticationSuccess',
            'security.authentication.faillure' =>
                'onSecurityAuthenticationFaillure',
        ];
    }

    public function onSecurityAuthenticationSuccess(
        AuthenticationSuccessEvent $event,
    ): void {
        $session = new Session();
        $session->getFlashBag()->add('success', "Connexion réussie");
    }

    public function onSecurityAuthenticationFaillure(
        AuthenticationSuccessEvent $event,
    ): void {
    }
}

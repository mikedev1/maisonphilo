<?php

namespace App\Form;

use App\Entity\GlobalSearch;
use Symfony\Component\Form\AbstractType;
use App\Form\GlobalSearchAutocompleteField;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GlobalSearchType extends AbstractType
{
    public function buildForm(
        FormBuilderInterface $builder,
        array $options
    ): void {
        $builder->add('link', EntityType::class, [
            'class' => GlobalSearch::class,
            'label' => false,
            'choice_value' => 'path',
            'placeholder' => "Rechercher",
            'choice_label' => "link",
            'autocomplete' => true,
            'multiple' => false,
        ]);

        /*  ->add('link', GlobalSearchAutocompleteField::class, [
                'choice_value' => function (?GlobalSearch $entity){
                    return $entity ? $entity->getPath() : '';
                }

            ]); */
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => GlobalSearch::class,
        ]);
    }
}

<?php

namespace App\Form;

use App\Entity\User;
use App\Entity\Country;
use App\Form\CountryAutocompleteField;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Validator\Constraints\Image;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
          /*   ->add('email', EmailType::class, [
                'required' => false,
                'empty_data' => '',
                'constraints' => [
                    new Length([
                        'max' => 50,
                        'maxMessage' => 'Votre adresse mail ne doit pas dépasser {{ limit }} caractères',
                    ]),
                    new NotBlank([
                        "message" => "Votre adresse mail est obligatoire"
                    ])
                ]
            ]) */
            ->add('nickname', TextType::class, [
                'label' => 'Votre surnom',
                'required' => false,
                'constraints' => [
                    new Length([
                        'max' => 50,
                        'maxMessage' => 'Votre surnom ne doit pas dépasser {{ limit }} caractères',
                    ])
                ]
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Votre prénom',
                'required' => false,
                'constraints' => [
                    new Length([
                        'max' => 50,
                        'maxMessage' => 'Votre prénom ne doit pas dépasser {{ limit }} caractères',
                    ])
                ]
            ])
            ->add('lastName', TextType::class, [
                'required' => false,
                'label' => 'Votre nom de famille',
                'constraints' => [
                    new Length([
                        'max' => 50,
                        'maxMessage' => 'Votre nom de famille ne doit pas dépasser {{ limit }} caractères',
                    ])
                ]
            ])
   /*          ->add('imageFile', VichImageType::class, [
                'label' => 'Votre avatar',
                'mapped' => false,
                'constraints' => [
                    new  Image(
                        [
                            'maxSize' => '1M',
                            'maxSizeMessage' => 'Le poids de votre image ne doit dépasser {{limit}}'
                        ]
                    )
                ]
            ])
            ->add('imageName') */
            ->add('phoneNumber', TextType::class, [
                'required' => false,
                'label' => 'Numéro de téléphone',
                'constraints' => [
                    new Length([
                        'max' => 15,
                        'maxMessage' => 'Votre numéro de téléphone est trop long',
                    ])
                ]
            ])
            ->add('country', CountryAutocompleteField::class, [
                'class' => Country::class,
                'required' => false,
                'label' => 'Votre pays'
            ])
            ->add('validation', SubmitType::class, [
                'row_attr' => [
                    'class' => 'd-flex justify-content-end'
                ],
                'attr' => [
                    'class' => 'btn btn-success-800'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

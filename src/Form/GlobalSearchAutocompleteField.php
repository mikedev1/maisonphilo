<?php

namespace App\Form;

use App\Entity\GlobalSearch;
use App\Repository\GlobalSearchRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\UX\Autocomplete\Form\AsEntityAutocompleteField;
use Symfony\UX\Autocomplete\Form\ParentEntityAutocompleteType;

#[AsEntityAutocompleteField]
class GlobalSearchAutocompleteField extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'class' => GlobalSearch::class,
            'label' => false,
            'placeholder' => 'Rechercher',
            'choice_value' => function (?GlobalSearch $entity) {
                return $entity ? $entity->getPath() : '';
            }
        ]);
    }

    public function getParent(): string
    {
        return ParentEntityAutocompleteType::class;
    }
}

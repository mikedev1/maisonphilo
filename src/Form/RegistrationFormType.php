<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'required' => false,
                'label' => 'Votre email',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Veuillez saisir votre mail'
                    ])
                ],
                'row_attr' => [
                    'class' => 'form-floating mb-3'
                ],
                'attr' => [
                    'class' => 'form-control border border-success',
                    'placeholder' => 'Votre email'
                ],
            ])

            ->add('plainPassword', PasswordType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'mapped' => false,
                'label' => 'Veuillez creer un mot de passe',
                'required' => false,
                'row_attr' => [
                    'class' => 'form-floating mb-3 border'
                ],
                'attr' => [
                    'autocomplete' => 'new-password',
                    'class' => 'form-control border-success',
                    'placeholder' => 'Veuillez creer un mot de passe'
                ],
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please enter a password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Your password should be at least {{ limit }} characters',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                    ]),
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label' => 'Veuillez accepter nos Conditions générales',
                'required' => false,
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter nos conditions générales de vente',
                    ]),
                ],
            ])

            ->add('submit', SubmitType::class, [
                'row_attr' => [
                    'class' => 'd-grid gap-2 mx-auto'
                ],
                'label' => 'Inscription',
                'attr' => [
                    'class' => 'btn-lg btn-success-800'
                ]
            ])

        /*     ->add('passwordConfirm', PasswordType::class, [
                'mapped' => false,
                'label' => 'Veuillez confirmer votre mot de passe',
                'row_attr' => [
                    'class' => 'form-floating mb-3 border'
                ],
                'attr' => [
                    'class' => 'form-control border-success',
                    'placeholder' => 'Veuillez confirmer votre mot de passe'
                ],
            ]) */

            ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}

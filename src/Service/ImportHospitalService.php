<?php

namespace App\Service;

use App\Entity\Hospital;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use App\Service\CsvTraitService;
use App\Repository\HospitalRepository;
use App\Repository\QuartierRepository;
use App\Repository\ServicePlatformRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportHospitalService
{
    public function __construct(
        private HospitalRepository $hospitalRepository,
        private EntityManagerInterface $entityManager,
        private CsvTraitService $csvTraitService,
        private ImportData $importData

    ) {
        $this->hospitalRepository = $hospitalRepository;
        $this->entityManager = $entityManager;
        $this->csvTraitService = $csvTraitService;
        $this->importData = $importData;
    }
    public function importHospital(SymfonyStyle $io): void
    {
        $csv = $this->csvTraitService->readCsvFile($io, 'hopitauxAbidjan');

        foreach ($csv as $arrayHospitaL) {
            $io->progressAdvance();
            $hospital = $this->createOrUpdate($arrayHospitaL);
            $this->entityManager->persist($hospital);
        }
        $this->entityManager->flush();

        $io->progressFinish();
        $io->success("L'importation est terminée");
    }

    public function createOrUpdate(array $arrayHospital): Hospital
    {
        foreach ($arrayHospital as $key => $val) {
            $value =  explode(";", $val);

            /* $value 
                O => city
                1 => quartier
                2 => nom de l'hopital
                3 => Telephone
                4 => Information
            */
            /* Slug city */
            /**
             * output => les 4 premieres lettres du string
             */

            /* Pays */
            $country = $this->importData->importCountry("Côte d'\Ivoire", "civ");
            $this->entityManager->persist($country);
            /* Service platforme  */
            $service = $this->importData->importServicePlatform("app_assistance_hospital");

            /* Ville */
            $city = $this->importData->importCity($value[0], $country);
            $this->entityManager->persist($city);

            /* Quartier */
            $quartier = $this->importData->importQuartier($value[1]);
            $this->entityManager->persist($quartier);

            /* Hospital */
            $hospital = $this->hospitalRepository->findOneBy(['name' => trim($value[2])]);
            if (!$hospital) {
                $hospital = new Hospital();
            }

            $this->entityManager->flush();
            $hospital
                ->setCity($city)
                ->setQuartier($quartier)
                ->setName(trim($value[2]))
                ->setPhoneNumber(trim($value[3]))
                ->setInformation(trim($value[4]))
                ->setServicePlatform($service)
                ->setIsGeriatric(false);
        }
        return $hospital;
    }
}

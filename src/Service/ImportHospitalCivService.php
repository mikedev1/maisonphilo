<?php

namespace App\Service;

use App\Entity\Hospital;
use App\Repository\HospitalRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ImportHospitalCivService
{

    public function __construct(
        private EntityManagerInterface $entityManager,
        private HospitalRepository $hospitalRepository,
        private CsvTraitService $csvTraitService,
        private ImportData $importData

    ) {
        $this->entityManager = $entityManager;
        $this->hospitalRepository = $hospitalRepository;
        $this->csvTraitService = $csvTraitService;
        $this->importData = $importData;
    }
    /**
     * Importation des hopitaux d'Abidjan depuis le fichier csv
     *
     * @param SymfonyStyle $io
     * @return void
     */
    public function ImportHospitalCiv(SymfonyStyle $io): void
    {
        $io->title('Importation des hopitaux interieurs de côte d\'Ivoire');

        $csv = $this->csvTraitService->readCsvFile($io, 'hopitauxCiv');

        foreach ($csv as $arrayHospital) {
            $io->progressAdvance();
            $hospital = $this->createOrUpdate($arrayHospital);
            $this->entityManager->persist($hospital);
        }
        $this->entityManager->flush();

        $io->progressFinish();
        $io->success("L'importation est terminée");
    }


    public function createOrUpdate(array $arrayHospital): Hospital
    {
        foreach ($arrayHospital as $key => $val) {
            $value =  explode(";", $val);

            /* $value
                0 => ville
                1 => Nom 
                2 => Telephone
                3 => Informations
            */

            /* Pays */
            $country = $this->importData->importCountry('Côte d\'Ivoire', 'civ');
            $this->entityManager->persist($country);

            /* Service de la plateforme */
            $service = $this->importData->importServicePlatform('app_assistance_hospital');

            /* Ville */
            $city = $this->importData->importCity($value[0], $country);
            $this->entityManager->persist($city);

            /* Hosptal */
            $hospital = $this->hospitalRepository->findOneBy(['name' => $value[1]]);
            if (!$hospital) {
                $hospital = new Hospital();
            }
            $hospital
                ->setCity($city)
                ->setName($value[1])
                ->setPhoneNumber($value[2])
                ->setInformation($value[3])
                ->setServicePlatform($service)
                ->setIsGeriatric(false);
        }
        return $hospital;
    }
}

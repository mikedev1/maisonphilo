<?php

namespace App\Service;

use League\Csv\Reader;
use Symfony\Component\Console\Style\SymfonyStyle;

final class CsvTraitService
{
    public function __construct(
    ) {
    }

    public function readCsvFile(
        SymfonyStyle $io,
        $csvFilePath,
        string $title = "Importation de données depuis un fichier csv",
    ) {
        $csvDatas = Reader::createFromPath('%kernel.root_dir%/../public/doc/' . $csvFilePath . '.csv', 'r');
        $csvDatas->setHeaderOffset(0);
        $io->title($title);

        $io->progressStart(count($csvDatas));

        return $csvDatas;
    }
}

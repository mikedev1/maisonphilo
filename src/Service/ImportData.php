<?php

namespace App\Service;

use App\Entity\City;
use App\Entity\Country;
use App\Entity\Quartier;
use Doctrine\ORM\EntityManager;
use App\Repository\CityRepository;
use App\Repository\CountryRepository;
use App\Repository\HospitalRepository;
use App\Repository\QuartierRepository;
use App\Repository\ServicePlatformRepository;

class ImportData
{
    public function __construct(
        private HospitalRepository $hospitalRepository,
        private CsvTraitService $csvTraitService,
        private QuartierRepository $quartierRepository,
        private CountryRepository $countryRepository,
        private CityRepository $cityRepository,
        private ServicePlatformRepository $servicePlatformRepository,
        
    )
    {
        $this->countryRepository = $countryRepository;
        $this->csvTraitService = $csvTraitService;
        $this->hospitalRepository = $hospitalRepository;
        $this->countryRepository = $countryRepository;
        $this->cityRepository = $cityRepository;
        $this->servicePlatformRepository = $servicePlatformRepository;
    }

    public function importCountry($name, $slug )
    {
        $country = $this->countryRepository->findOneBy(['slug' => $slug]);
        if (!$country) {
            $country = new Country;
            $country
                ->setName($name)
                ->setSlug($slug);
        }

        return $country;
    }

    public function importServicePlatform(string $name){
        $service = $this->servicePlatformRepository->findOneBy(['path'=> $name]);

        return $service;
    }

    public function importCity($value, $country){
        $city = $this->cityRepository->findOneBy(['name' =>strtolower($value)]);
        if (!$city) {
            $city = new City();
        }
        $city
            ->setName(trim($value))
            ->setSlug( strtolower(trim(substr($value, 0, 3))) )
            ->setCountry($country);

        return $city;
    }

    public function importQuartier($value){
        $quartier = $this->quartierRepository->findOneBy(['name' =>  trim($value)]);
        if (!$quartier) {
            $quartier = new Quartier();
        }
        $quartier
            ->setName(trim($value));

        return $quartier;
    }
}

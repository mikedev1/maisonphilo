<?php

namespace App\Service;

use App\Entity\Clinique;
use App\Repository\CliniqueRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class ImportCliniqueService
{
    public function __construct(
        private CsvTraitService $csvTraitService,
        private EntityManagerInterface $entityManager,
        private ImportData $importData,
        private CliniqueRepository $cliniqueRepository
    ) {
        $this->csvTraitService = $csvTraitService;
        $this->entityManager = $entityManager;
        $this->importData = $importData;
        $this->cliniqueRepository = $cliniqueRepository;
    }

    public function import(SymfonyStyle $io)
    {
        $csv = $this->csvTraitService->readCsvFile($io, 'cliniqueAbidjan');

        foreach ($csv as $arrayClinique) {
            $io->progressAdvance();
            $clinique = $this->createOrUpdate($arrayClinique);

            $this->entityManager->persist(
                $clinique
            );
        }
        $this->entityManager->flush();

        $io->progressFinish();
        $io->success("L'importation est terminée");
    }


    public function createOrUpdate(array $arrayClinique): Clinique
    {
        foreach ($arrayClinique as $key => $val) {
            $value =  explode(";", $val);

            /* $value 
                0 => quartier
                1 => nom de l'hopital
                2 => Telephone
                3 => Information
            */

            /* Pays */
            $country = $this->importData->importCountry('Côte d\'Ivoire', 'civ');

            /* Quartier */
            $quartier = $this->importData->importQuartier($value[0]);
            $this->entityManager->persist($quartier);

            /* City */
            $city = $this->importData->importCity("Abidjan", $country);
            $this->entityManager->persist($city);

            $clinique = $this->cliniqueRepository->findOneBy(['name' => $value[1]]);
            if (!$clinique) {
                $clinique = new Clinique();
            }
            $clinique
                ->setCity($city)
                ->setQuartier($quartier)
                ->setName($value[1])                ->setPhoneNumber($value[2])
                ->setInformation($value[3])
                ->setCountry($country);
            $this->entityManager->persist($clinique);
        }
        $this->entityManager->flush();

        return $clinique;
    }
}

<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;



class   PaginationService extends AbstractController
{
    public function paginationService(
        EntityManagerInterface $em,
        PaginatorInterface $paginator,
        Request $request
    ) {
        $dql   = "SELECT h FROM App\Entity\Hospital h";
        $query = $em->createQuery($dql);

        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        // parameters to template
        return $pagination;
    }
}

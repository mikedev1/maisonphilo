<?php

namespace App\Service;

use App\Entity\City;
use App\Entity\Clinique;
use App\Entity\Quartier;
use App\Repository\CityRepository;
use App\Repository\CliniqueRepository;
use App\Repository\CountryRepository;
use App\Repository\QuartierRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class ImportCliniqueCivService
{
    public function __construct(
        private CsvTraitService $csvTraitService,
        private CliniqueRepository $cliniqueRepository,
        private EntityManagerInterface $entityManager,
        private ImportData $importData,
       
    ) {
        $this->csvTraitService = $csvTraitService;
        $this->importData = $importData;
      
    }

    public function import(SymfonyStyle $io)
    {
        $csv = $this->csvTraitService->readCsvFile($io, 'cliniqueCiv');

        foreach ($csv as $arrayClinique) {
            $io->progressAdvance();
            $clinique = $this->createOrUpdate($arrayClinique);

            $this->entityManager->persist(
                $clinique
            );
        }
        $this->entityManager->flush();

        $io->progressFinish();
        $io->success("L'importation est terminée");
    }


    public function createOrUpdate(array $arrayClinique): Clinique
    {
        foreach ($arrayClinique as $val) {
            $value =  explode(";", $val);

            /* $value 
                0 => Ville
                1 => nom de la clinique
                2 => Telephone
                3 => Information
            */


            /* Pays */
            $country = $this->importData->importCountry('Côte d\'Ivoire', 'civ');

            /* Ville */
            $city = $this->importData->importCity($value[0], $country);
            $this->entityManager->persist($city);


            $clinique = $this->cliniqueRepository->findOneBy(['name' => $value[1]]);
            if (!$clinique) {
                $clinique = new Clinique();
            }
            $clinique
                ->setCity($city)
                ->setName($value[1])
                ->setPhoneNumber($value[2])
                ->setInformation($value[3])
                ->setCountry($country);
            $this->entityManager->persist($clinique);
        }
        $this->entityManager->flush();

        return $clinique;
    }
}

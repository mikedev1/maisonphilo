<?php
namespace App\Components;

use Symfony\UX\TwigComponent\Attribute\AsTwigComponent;


#[AsTwigComponent('pharmacyComponent')]
class PharmacyComponent
{
    public $pharmacy;
    public $country;
    public $id;
}
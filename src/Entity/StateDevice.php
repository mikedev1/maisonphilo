<?php

namespace App\Entity;

use App\Repository\StateDeviceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StateDeviceRepository::class)
 */
class StateDevice
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $links = [];

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="stateDevices")
     * @ORM\JoinColumn(nullable=false)
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity=ServicePlatform::class, inversedBy="stateDevices")
     */
    private $servicePlatform;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getdescription(): ?string
    {
        return $this->description;
    }

    public function setdescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getlinks(): ?array
    {
        return $this->links;
    }

    public function setlinks(?array $links): self
    {
        $this->links = $links;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getServicePlatform(): ?ServicePlatform
    {
        return $this->servicePlatform;
    }

    public function setServicePlatform(?ServicePlatform $servicePlatform): self
    {
        $this->servicePlatform = $servicePlatform;

        return $this;
    }
}

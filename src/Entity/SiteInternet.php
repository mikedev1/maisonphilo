<?php

namespace App\Entity;

use App\Repository\SiteInternetRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\UX\Turbo\Attribute\Broadcast;

/**
 * @ORM\Entity(repositoryClass=SiteInternetRepository::class)
 */
#[Broadcast]
class SiteInternet
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=country::class, inversedBy="siteInternets")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity=ServicePlatform::class, inversedBy="siteInternets")
     */
    private $servicePlatform;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCountry(): ?country
    {
        return $this->country;
    }

    public function setCountry(?country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getServicePlatform(): ?ServicePlatform
    {
        return $this->servicePlatform;
    }

    public function setServicePlatform(?ServicePlatform $servicePlatform): self
    {
        $this->servicePlatform = $servicePlatform;

        return $this;
    }
}

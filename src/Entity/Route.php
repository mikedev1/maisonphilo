<?php

namespace App\Entity;

use App\Repository\RouteRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RouteRepository::class)
 */
class Route
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $path;

    /**
     * @ORM\OneToMany(targetEntity=GlobalSearch::class, mappedBy="path", orphanRemoval=true)
     */
    private $globalSearches;

    /**
     * @ORM\OneToMany(targetEntity=Commerces::class, mappedBy="category")
     */
    private $commerces;

    public function __construct()
    {
        $this->globalSearches = new ArrayCollection();
        $this->commerces = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return Collection<int, GlobalSearch>
     */
    public function getGlobalSearches(): Collection
    {
        return $this->globalSearches;
    }

    public function addGlobalSearch(GlobalSearch $globalSearch): self
    {
        if (!$this->globalSearches->contains($globalSearch)) {
            $this->globalSearches[] = $globalSearch;
            $globalSearch->setPath($this);
        }

        return $this;
    }

    public function removeGlobalSearch(GlobalSearch $globalSearch): self
    {
        if ($this->globalSearches->removeElement($globalSearch)) {
            // set the owning side to null (unless already changed)
            if ($globalSearch->getPath() === $this) {
                $globalSearch->setPath(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getPath();
    }

    /**
     * @return Collection<int, Commerces>
     */
    public function getCommerces(): Collection
    {
        return $this->commerces;
    }

    public function addCommerce(Commerces $commerce): self
    {
        if (!$this->commerces->contains($commerce)) {
            $this->commerces[] = $commerce;
            $commerce->setCategory($this);
        }

        return $this;
    }

    public function removeCommerce(Commerces $commerce): self
    {
        if ($this->commerces->removeElement($commerce)) {
            // set the owning side to null (unless already changed)
            if ($commerce->getCategory() === $this) {
                $commerce->setCategory(null);
            }
        }

        return $this;
    }
}

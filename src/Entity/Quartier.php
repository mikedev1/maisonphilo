<?php

namespace App\Entity;

use App\Repository\QuartierRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=QuartierRepository::class)
 */
class Quartier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=City::class, mappedBy="quartier")
     */
    private $cities;

    /**
     * @ORM\OneToMany(targetEntity=Clinique::class, mappedBy="quartier")
     */
    private $cliniques;

    /**
     * @ORM\OneToMany(targetEntity=Hospital::class, mappedBy="quartier")
     */
    private $hospitals;

    public function __construct()
    {
        $this->cities = new ArrayCollection();
        $this->cliniques = new ArrayCollection();
        $this->hospitals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, City>
     */
    public function getCities(): Collection
    {
        return $this->cities;
    }

    public function addCity(City $city): self
    {
        if (!$this->cities->contains($city)) {
            $this->cities[] = $city;
            $city->setQuartier($this);
        }

        return $this;
    }

    public function removeCity(City $city): self
    {
        if ($this->cities->removeElement($city)) {
            // set the owning side to null (unless already changed)
            if ($city->getQuartier() === $this) {
                $city->setQuartier(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return Collection<int, Clinique>
     */
    public function getCliniques(): Collection
    {
        return $this->cliniques;
    }

    public function addClinique(Clinique $clinique): self
    {
        if (!$this->cliniques->contains($clinique)) {
            $this->cliniques[] = $clinique;
            $clinique->setQuartier($this);
        }

        return $this;
    }

    public function removeClinique(Clinique $clinique): self
    {
        if ($this->cliniques->removeElement($clinique)) {
            // set the owning side to null (unless already changed)
            if ($clinique->getQuartier() === $this) {
                $clinique->setQuartier(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Hospital>
     */
    public function getHospitals(): Collection
    {
        return $this->hospitals;
    }

    public function addHospital(Hospital $hospital): self
    {
        if (!$this->hospitals->contains($hospital)) {
            $this->hospitals[] = $hospital;
            $hospital->setQuartier($this);
        }

        return $this;
    }

    public function removeHospital(Hospital $hospital): self
    {
        if ($this->hospitals->removeElement($hospital)) {
            // set the owning side to null (unless already changed)
            if ($hospital->getQuartier() === $this) {
                $hospital->setQuartier(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\GlobalSearchRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GlobalSearchRepository::class)
 */
class GlobalSearch
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity=Route::class, inversedBy="globalSearches")
     * @ORM\JoinColumn(nullable=false)
     */
    private $path;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLink(): ?string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getPath(): ?Route
    {
        return $this->path;
    }

    public function setPath(?Route $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function __toString()
    {
        return $this->getLink();
    }
}

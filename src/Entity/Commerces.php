<?php

namespace App\Entity;

use App\Repository\CommercesRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\UX\Turbo\Attribute\Broadcast;

/**
 * @ORM\Entity(repositoryClass=CommercesRepository::class)
 */
#[Broadcast]
class Commerces
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="commerces")
     */
    private $country;

    /**
     * @ORM\ManyToOne(targetEntity=Route::class, inversedBy="commerces")
     */
    private $category;

    /**
     * @ORM\ManyToOne(targetEntity=ServicePlatform::class, inversedBy="commerces")
     */
    private $servicePlatform;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCategory(): ?Route
    {
        return $this->category;
    }

    public function setCategory(?Route $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getServicePlatform(): ?ServicePlatform
    {
        return $this->servicePlatform;
    }

    public function setServicePlatform(?ServicePlatform $servicePlatform): self
    {
        $this->servicePlatform = $servicePlatform;

        return $this;
    }
}

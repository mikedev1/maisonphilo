<?php

namespace App\Entity;

use App\Repository\CityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CityRepository::class)
 */
class City
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity=Country::class, inversedBy="cities")
     */
    private $country;

    /**
     * @ORM\OneToMany(targetEntity=Clinique::class, mappedBy="city")
     */
    private $cliniques;

    /**
     * @ORM\ManyToOne(targetEntity=Quartier::class, inversedBy="cities")
     */
    private $quartier;

    /**
     * @ORM\OneToMany(targetEntity=Hospital::class, mappedBy="city")
     */
    private $hospitals;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    public function __construct()
    {
        $this->cliniques = new ArrayCollection();
        $this->hospitals = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getCountry(): ?Country
    {
        return $this->country;
    }

    public function setCountry(?Country $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * @return Collection<int, Clinique>
     */
    public function getCliniques(): Collection
    {
        return $this->cliniques;
    }

    public function addClinique(Clinique $clinique): self
    {
        if (!$this->cliniques->contains($clinique)) {
            $this->cliniques[] = $clinique;
            $clinique->setCity($this);
        }

        return $this;
    }

    public function removeClinique(Clinique $clinique): self
    {
        if ($this->cliniques->removeElement($clinique)) {
            // set the owning side to null (unless already changed)
            if ($clinique->getCity() === $this) {
                $clinique->setCity(null);
            }
        }

        return $this;
    }

    public function getQuartier(): ?Quartier
    {
        return $this->quartier;
    }

    public function setQuartier(?Quartier $quartier): self
    {
        $this->quartier = $quartier;

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return Collection<int, Hospital>
     */
    public function getHospitals(): Collection
    {
        return $this->hospitals;
    }

    public function addHospital(Hospital $hospital): self
    {
        if (!$this->hospitals->contains($hospital)) {
            $this->hospitals[] = $hospital;
            $hospital->setCity($this);
        }

        return $this;
    }

    public function removeHospital(Hospital $hospital): self
    {
        if ($this->hospitals->removeElement($hospital)) {
            // set the owning side to null (unless already changed)
            if ($hospital->getCity() === $this) {
                $hospital->setCity(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }
}

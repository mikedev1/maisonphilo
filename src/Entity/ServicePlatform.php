<?php

namespace App\Entity;

use App\Repository\ServicePlatformRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ServicePlatformRepository::class)
 */
class ServicePlatform
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isValid;

    /**
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $orderList;

    /**
     * @ORM\OneToMany(targetEntity=Hospital::class, mappedBy="servicePlatform")
     */
    private $hospitals;

    /**
     * @ORM\OneToMany(targetEntity=Partner::class, mappedBy="servicePlatform")
     */
    private $partners;

    /**
     * @ORM\OneToMany(targetEntity=StateDevice::class, mappedBy="servicePlatform")
     */
    private $stateDevices;

    /**
     * @ORM\OneToMany(targetEntity=SocialCenter::class, mappedBy="servicePlatform")
     */
    private $socialCenters;

    /**
     * @ORM\OneToMany(targetEntity=SiteInternet::class, mappedBy="servicePlatform")
     */
    private $siteInternets;

    /**
     * @ORM\OneToMany(targetEntity=Commerces::class, mappedBy="servicePlatform")
     */
    private $commerces;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isNotWorking;

    /**
     * @ORM\OneToMany(targetEntity=Pharmacy::class, mappedBy="servicePlatform")
     */
    private $pharmacies;

    public function __construct()
    {
        $this->hospitals = new ArrayCollection();
        $this->partners = new ArrayCollection();
        $this->stateDevices = new ArrayCollection();
        $this->socialCenters = new ArrayCollection();
        $this->siteInternets = new ArrayCollection();
        $this->commerces = new ArrayCollection();
        $this->pharmacies = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPath(): ?string
    {
        return $this->path;
    }

    public function setPath(?string $path): self
    {
        $this->path = $path;

        return $this;
    }

    public function isIsValid(): ?bool
    {
        return $this->isValid;
    }

    public function setIsValid(bool $isValid): self
    {
        $this->isValid = $isValid;

        return $this;
    }

    public function getOrderList(): ?int
    {
        return $this->orderList;
    }

    public function setOrderList(?int $orderList): self
    {
        $this->orderList = $orderList;

        return $this;
    }

    /**
     * @return Collection<int, Hospital>
     */
    public function getHospitals(): Collection
    {
        return $this->hospitals;
    }

    public function addHospital(Hospital $hospital): self
    {
        if (!$this->hospitals->contains($hospital)) {
            $this->hospitals[] = $hospital;
            $hospital->setServicePlatform($this);
        }

        return $this;
    }

    public function removeHospital(Hospital $hospital): self
    {
        if ($this->hospitals->removeElement($hospital)) {
            // set the owning side to null (unless already changed)
            if ($hospital->getServicePlatform() === $this) {
                $hospital->setServicePlatform(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getName();
    }

    /**
     * @return Collection<int, Partner>
     */
    public function getPartners(): Collection
    {
        return $this->partners;
    }

    public function addPartner(Partner $partner): self
    {
        if (!$this->partners->contains($partner)) {
            $this->partners[] = $partner;
            $partner->setServicePlatform($this);
        }

        return $this;
    }

    public function removePartner(Partner $partner): self
    {
        if ($this->partners->removeElement($partner)) {
            // set the owning side to null (unless already changed)
            if ($partner->getServicePlatform() === $this) {
                $partner->setServicePlatform(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, StateDevice>
     */
    public function getStateDevices(): Collection
    {
        return $this->stateDevices;
    }

    public function addStateDevice(StateDevice $stateDevice): self
    {
        if (!$this->stateDevices->contains($stateDevice)) {
            $this->stateDevices[] = $stateDevice;
            $stateDevice->setServicePlatform($this);
        }

        return $this;
    }

    public function removeStateDevice(StateDevice $stateDevice): self
    {
        if ($this->stateDevices->removeElement($stateDevice)) {
            // set the owning side to null (unless already changed)
            if ($stateDevice->getServicePlatform() === $this) {
                $stateDevice->setServicePlatform(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SocialCenter>
     */
    public function getSocialCenters(): Collection
    {
        return $this->socialCenters;
    }

    public function addSocialCenter(SocialCenter $socialCenter): self
    {
        if (!$this->socialCenters->contains($socialCenter)) {
            $this->socialCenters[] = $socialCenter;
            $socialCenter->setServicePlatform($this);
        }

        return $this;
    }

    public function removeSocialCenter(SocialCenter $socialCenter): self
    {
        if ($this->socialCenters->removeElement($socialCenter)) {
            // set the owning side to null (unless already changed)
            if ($socialCenter->getServicePlatform() === $this) {
                $socialCenter->setServicePlatform(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SiteInternet>
     */
    public function getSiteInternets(): Collection
    {
        return $this->siteInternets;
    }

    public function addSiteInternet(SiteInternet $siteInternet): self
    {
        if (!$this->siteInternets->contains($siteInternet)) {
            $this->siteInternets[] = $siteInternet;
            $siteInternet->setServicePlatform($this);
        }

        return $this;
    }

    public function removeSiteInternet(SiteInternet $siteInternet): self
    {
        if ($this->siteInternets->removeElement($siteInternet)) {
            // set the owning side to null (unless already changed)
            if ($siteInternet->getServicePlatform() === $this) {
                $siteInternet->setServicePlatform(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Commerces>
     */
    public function getCommerces(): Collection
    {
        return $this->commerces;
    }

    public function addCommerce(Commerces $commerce): self
    {
        if (!$this->commerces->contains($commerce)) {
            $this->commerces[] = $commerce;
            $commerce->setServicePlatform($this);
        }

        return $this;
    }

    public function removeCommerce(Commerces $commerce): self
    {
        if ($this->commerces->removeElement($commerce)) {
            // set the owning side to null (unless already changed)
            if ($commerce->getServicePlatform() === $this) {
                $commerce->setServicePlatform(null);
            }
        }

        return $this;
    }

    public function isisNotWorking(): ?bool
    {
        return $this->isNotWorking;
    }

    public function setisNotWorking(?bool $isNotWorking): self
    {
        $this->isNotWorking = $isNotWorking;

        return $this;
    }

    /**
     * @return Collection<int, Pharmacy>
     */
    public function getPharmacies(): Collection
    {
        return $this->pharmacies;
    }

    public function addPharmacy(Pharmacy $pharmacy): self
    {
        if (!$this->pharmacies->contains($pharmacy)) {
            $this->pharmacies[] = $pharmacy;
            $pharmacy->setServicePlatform($this);
        }

        return $this;
    }

    public function removePharmacy(Pharmacy $pharmacy): self
    {
        if ($this->pharmacies->removeElement($pharmacy)) {
            // set the owning side to null (unless already changed)
            if ($pharmacy->getServicePlatform() === $this) {
                $pharmacy->setServicePlatform(null);
            }
        }

        return $this;
    }
}

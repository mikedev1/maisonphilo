<?php

namespace App\Entity;

use App\Repository\GeneralTermsRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=GeneralTermsRepository::class)
 */
class GeneralTerms
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $legalNotice;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $cookiePolicy;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $protectionOfPrivacy;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLegalNotice(): ?string
    {
        return $this->legalNotice;
    }

    public function setLegalNotice(?string $legalNotice): self
    {
        $this->legalNotice = $legalNotice;

        return $this;
    }

    public function getCookiePolicy(): ?string
    {
        return $this->cookiePolicy;
    }

    public function setCookiePolicy(?string $cookiePolicy): self
    {
        $this->cookiePolicy = $cookiePolicy;

        return $this;
    }

    public function getProtectionOfPrivacy(): ?string
    {
        return $this->protectionOfPrivacy;
    }

    public function setProtectionOfPrivacy(?string $protectionOfPrivacy): self
    {
        $this->protectionOfPrivacy = $protectionOfPrivacy;

        return $this;
    }
}

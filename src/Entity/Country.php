<?php

namespace App\Entity;

use App\Repository\CountryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CountryRepository::class)
 */
class Country
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Partner::class, mappedBy="country")
     */
    private $partners;

    /**
     * @ORM\OneToMany(targetEntity=Product::class, mappedBy="country")
     */
    private $products;

    /**
     * @ORM\ManyToMany(targetEntity=Contact::class, mappedBy="country")
     */
    private $contacts;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isValid;

    /**
     * @ORM\OneToMany(targetEntity=StateDevice::class, mappedBy="country")
     */
    private $stateDevices;

    /**
     * @ORM\OneToMany(targetEntity=SocialCenter::class, mappedBy="country")
     */
    private $socialCenters;

    /**
     * @ORM\OneToMany(targetEntity=User::class, mappedBy="country")
     */
    private $users;

    /**
     * @ORM\OneToMany(targetEntity=Commerces::class, mappedBy="country")
     */
    private $commerces;

    /**
     * @ORM\OneToMany(targetEntity=SiteInternet::class, mappedBy="country")
     */
    private $siteInternets;

    /**
     * @ORM\OneToMany(targetEntity=Pharmacy::class, mappedBy="country")
     */
    private $pharmacies;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $slug;

    /**
     * @ORM\OneToMany(targetEntity=City::class, mappedBy="country")
     */
    private $cities;

    /**
     * @ORM\OneToMany(targetEntity=Clinique::class, mappedBy="country")
     */
    private $cliniques;

    public function __construct()
    {
        $this->partners = new ArrayCollection();
        $this->products = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->stateDevices = new ArrayCollection();
        $this->socialCenters = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->commerces = new ArrayCollection();
        $this->siteInternets = new ArrayCollection();
        $this->pharmacies = new ArrayCollection();
        $this->cities = new ArrayCollection();
        $this->cliniques = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection<int, Partner>
     */
    public function getPartners(): Collection
    {
        return $this->partners;
    }

    public function addPartner(Partner $partner): self
    {
        if (!$this->partners->contains($partner)) {
            $this->partners->add($partner);
            $partner->setCountry($this);
        }

        return $this;
    }

    public function removePartner(Partner $partner): self
    {
        if ($this->partners->removeElement($partner)) {
            // set the owning side to null (unless already changed)
            if ($partner->getCountry() === $this) {
                $partner->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Product>
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products->add($product);
            $product->setCountry($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->removeElement($product)) {
            // set the owning side to null (unless already changed)
            if ($product->getCountry() === $this) {
                $product->setCountry(null);
            }
        }

        return $this;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    /**
     * @return Collection<int, Contact>
     */
    public function getContacts(): Collection
    {
        return $this->contacts;
    }

    public function addContact(Contact $contact): self
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
            $contact->addCountry($this);
        }

        return $this;
    }

    public function removeContact(Contact $contact): self
    {
        if ($this->contacts->removeElement($contact)) {
            $contact->removeCountry($this);
        }

        return $this;
    }

    public function isIsValid(): ?bool
    {
        return $this->isValid;
    }

    public function setIsValid(bool $isValid): self
    {
        $this->isValid = $isValid;

        return $this;
    }

    /**
     * @return Collection<int, StateDevice>
     */
    public function getStateDevices(): Collection
    {
        return $this->stateDevices;
    }

    public function addStateDevice(StateDevice $stateDevice): self
    {
        if (!$this->stateDevices->contains($stateDevice)) {
            $this->stateDevices[] = $stateDevice;
            $stateDevice->setCountry($this);
        }

        return $this;
    }

    public function removeStateDevice(StateDevice $stateDevice): self
    {
        if ($this->stateDevices->removeElement($stateDevice)) {
            // set the owning side to null (unless already changed)
            if ($stateDevice->getCountry() === $this) {
                $stateDevice->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SocialCenter>
     */
    public function getSocialCenters(): Collection
    {
        return $this->socialCenters;
    }

    public function addSocialCenter(SocialCenter $socialCenter): self
    {
        if (!$this->socialCenters->contains($socialCenter)) {
            $this->socialCenters[] = $socialCenter;
            $socialCenter->setCountry($this);
        }

        return $this;
    }

    public function removeSocialCenter(SocialCenter $socialCenter): self
    {
        if ($this->socialCenters->removeElement($socialCenter)) {
            // set the owning side to null (unless already changed)
            if ($socialCenter->getCountry() === $this) {
                $socialCenter->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(User $user): self
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
            $user->setCountry($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->users->removeElement($user)) {
            // set the owning side to null (unless already changed)
            if ($user->getCountry() === $this) {
                $user->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Commerces>
     */
    public function getCommerces(): Collection
    {
        return $this->commerces;
    }

    public function addCommerce(Commerces $commerce): self
    {
        if (!$this->commerces->contains($commerce)) {
            $this->commerces[] = $commerce;
            $commerce->setCountry($this);
        }

        return $this;
    }

    public function removeCommerce(Commerces $commerce): self
    {
        if ($this->commerces->removeElement($commerce)) {
            // set the owning side to null (unless already changed)
            if ($commerce->getCountry() === $this) {
                $commerce->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, SiteInternet>
     */
    public function getSiteInternets(): Collection
    {
        return $this->siteInternets;
    }

    public function addSiteInternet(SiteInternet $siteInternet): self
    {
        if (!$this->siteInternets->contains($siteInternet)) {
            $this->siteInternets[] = $siteInternet;
            $siteInternet->setCountry($this);
        }

        return $this;
    }

    public function removeSiteInternet(SiteInternet $siteInternet): self
    {
        if ($this->siteInternets->removeElement($siteInternet)) {
            // set the owning side to null (unless already changed)
            if ($siteInternet->getCountry() === $this) {
                $siteInternet->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Pharmacy>
     */
    public function getPharmacies(): Collection
    {
        return $this->pharmacies;
    }

    public function addPharmacy(Pharmacy $pharmacy): self
    {
        if (!$this->pharmacies->contains($pharmacy)) {
            $this->pharmacies[] = $pharmacy;
            $pharmacy->setCountry($this);
        }

        return $this;
    }

    public function removePharmacy(Pharmacy $pharmacy): self
    {
        if ($this->pharmacies->removeElement($pharmacy)) {
            // set the owning side to null (unless already changed)
            if ($pharmacy->getCountry() === $this) {
                $pharmacy->setCountry(null);
            }
        }

        return $this;
    }

    public function getSlug(): ?string
    {
        return $this->slug;
    }

    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return Collection<int, City>
     */
    public function getCities(): Collection
    {
        return $this->cities;
    }

    public function addCity(City $city): self
    {
        if (!$this->cities->contains($city)) {
            $this->cities[] = $city;
            $city->setCountry($this);
        }

        return $this;
    }

    public function removeCity(City $city): self
    {
        if ($this->cities->removeElement($city)) {
            // set the owning side to null (unless already changed)
            if ($city->getCountry() === $this) {
                $city->setCountry(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection<int, Clinique>
     */
    public function getCliniques(): Collection
    {
        return $this->cliniques;
    }

    public function addClinique(Clinique $clinique): self
    {
        if (!$this->cliniques->contains($clinique)) {
            $this->cliniques[] = $clinique;
            $clinique->setCountry($this);
        }

        return $this;
    }

    public function removeClinique(Clinique $clinique): self
    {
        if ($this->cliniques->removeElement($clinique)) {
            // set the owning side to null (unless already changed)
            if ($clinique->getCountry() === $this) {
                $clinique->setCountry(null);
            }
        }

        return $this;
    }
}

<?php

namespace App\Repository;

use App\Entity\HospitalCivInterieur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HospitalCivInterieur>
 *
 * @method HospitalCivInterieur|null find($id, $lockMode = null, $lockVersion = null)
 * @method HospitalCivInterieur|null findOneBy(array $criteria, array $orderBy = null)
 * @method HospitalCivInterieur[]    findAll()
 * @method HospitalCivInterieur[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HospitalCivInterieurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HospitalCivInterieur::class);
    }

    public function add(HospitalCivInterieur $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(HospitalCivInterieur $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return HospitalCivInterieur[] Returns an array of HospitalCivInterieur objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('h.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?HospitalCivInterieur
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

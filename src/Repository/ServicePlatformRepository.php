<?php

namespace App\Repository;

use App\Entity\ServicePlatform;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ServicePlatform>
 *
 * @method ServicePlatform|null find($id, $lockMode = null, $lockVersion = null)
 * @method ServicePlatform|null findOneBy(array $criteria, array $orderBy = null)
 * @method ServicePlatform[]    findAll()
 * @method ServicePlatform[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ServicePlatformRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ServicePlatform::class);
    }

    public function add(ServicePlatform $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(ServicePlatform $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    //    /**
    //     * @return ServicePlatform[] Returns an array of ServicePlatform objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('s')
    //            ->andWhere('s.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('s.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    public function findOneByServicePlatformName($value): ?ServicePlatform
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.examp
            
            
            
            leField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult();
    }
}

<?php

namespace App\Repository;

use App\Entity\Clinique;
use App\Entity\Country;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\City;

/**
 * @extends ServiceEntityRepository<Clinique>
 *
 * @method Clinique|null find($id, $lockMode = null, $lockVersion = null)
 * @method Clinique|null findOneBy(array $criteria, array $orderBy = null)
 * @method Clinique[]    findAll()
 * @method Clinique[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CliniqueRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Clinique::class);
    }

    public function add(Clinique $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(Clinique $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function findByCountry($city)
    {
        return $this->createQueryBuilder('c')
           
            ->andWhere('c.slug = :city')
          
            ->setParameter('city', $city)
            ->getQuery()
            ->getResult();
    }

    public function findByCity($slug){
        return $this->createQueryBuilder('clinique')
        ->join(City::class, 'c')
        ->where('c.slug = :slug')
        ->andWhere('c.id = clinique.city')
        ->setParameter('slug', $slug)
        ->getQuery()
        ->getresult();
    }

    //    /**
    //     * @return Clinique[] Returns an array of Clinique objects
    //     */
    //    public function findByExampleField($value): array
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->orderBy('c.id', 'ASC')
    //            ->setMaxResults(10)
    //            ->getQuery()
    //            ->getResult()
    //        ;
    //    }

    //    public function findOneBySomeField($value): ?Clinique
    //    {
    //        return $this->createQueryBuilder('c')
    //            ->andWhere('c.exampleField = :val')
    //            ->setParameter('val', $value)
    //            ->getQuery()
    //            ->getOneOrNullResult()
    //        ;
    //    }
}

<?php

namespace App\Repository;

use App\Entity\City;
use App\Entity\Country;
use App\Entity\Hospital;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Hospital|null find($id, $lockMode = null, $lockVersion = null)
 * @method Hospital|null findOneBy(array $criteria, array $orderBy = null)
 * @method Hospital[]    findAll()
 * @method Hospital[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HospitalRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Hospital::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Hospital $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Hospital $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    public function hospitalShow($country)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.isValid = :val', 'c.country =:country')
            ->setParameter('val', '1')
            ->setParameter('country', $country)
            ->getQuery()
            ->getResult();
    }

    public function hospitalShowByCountry($country)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.isValid = :val', 'c.country =:country')
            ->setParameter('val', '1')
            ->setParameter('country', $country)
            ->orderBy('c.id', 'ASC')
            ->getQuery()
            ->getResult();
    }


    public function findByCity($slug){
        return $this->createQueryBuilder('h')
        ->join(City::class, 'c')
        ->where('c.slug = :slug')
        ->andWhere('c.id = h.city')
        ->setParameter('slug', $slug)
        ->getQuery()
        ->getresult();
    }


    // /**
    //  * @return Hospital[] Returns an array of Hospital objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Hospital
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

<?php

namespace App\Repository;

use App\Entity\HospitalAbidjan;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<HospitalAbidjan>
 *
 * @method HospitalAbidjan|null find($id, $lockMode = null, $lockVersion = null)
 * @method HospitalAbidjan|null findOneBy(array $criteria, array $orderBy = null)
 * @method HospitalAbidjan[]    findAll()
 * @method HospitalAbidjan[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HospitalAbidjanRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, HospitalAbidjan::class);
    }

    public function add(HospitalAbidjan $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

    public function remove(HospitalAbidjan $entity, bool $flush = false): void
    {
        $this->getEntityManager()->remove($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }

//    /**
//     * @return HospitalAbidjan[] Returns an array of HospitalAbidjan objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('h.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?HospitalAbidjan
//    {
//        return $this->createQueryBuilder('h')
//            ->andWhere('h.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}

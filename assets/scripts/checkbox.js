import $ from "jquery";
// image gallery
// init the state from the input

$(() => {
  $(".image-checkbox").each(function checkbox() {
    if ($(this).find("input[type=\"checkbox\"]").first().attr("checked")) {
      $(this).addClass("image-checkbox-checked");
    } else {
      $(this).removeClass("image-checkbox-checked");
    }
  });

  // sync the state to the input
  $(".image-checkbox").on("click", function check(e) {
    $(this).toggleClass("image-checkbox-checked");
    const $checkbox = $(this).find("input[type=\"checkbox\"]");
    $checkbox.prop("checked", !$checkbox.prop("checked"));

    e.preventDefault();
  });
});

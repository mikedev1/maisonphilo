import $ from "jquery";

const parallax = $(".js-parallax");

$(window).on("scroll", () => {
  // console.log(window.scrollY);
  parallax.css("background-position-y", `${window.scrollY / -2}px`);
});

import $ from "jquery";

const mybutton = $("#my-btn");

function scrollFunction() {
  if (
    document.body.scrollTop > 300 ||
    document.documentElement.scrollTop > 300
  ) {
    // mybutton.css("display","block");
    mybutton.fadeIn();
  } else {
    mybutton.fadeOut();
  }
}

// When the user scrolls down 20px from the top of the document, show the button
window.onscroll = function scroll() {
  scrollFunction();
};

// When the user clicks on the button, scroll to the top of the document
mybutton.on("click", (e) => {
  e.preventDefault();
  document.body.scrollTop = 0; // For Safari
  document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
});

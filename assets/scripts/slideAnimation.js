import $ from "jquery";
// fade sliding animation in section engagement
$(() => {
  for (let i = 1; i <= 3; i += 1) {
    $(`.show${i}`).on("click", (e) => {
      e.preventDefault();
      $(`#show${i}`).slideToggle();
      $(`.hide${i}`).slideToggle();
    });
  }
});

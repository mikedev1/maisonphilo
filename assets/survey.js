import $ from "jquery";

import "./styles/survey.scss";

/* initialization of variables */

/* specialists */
const specialistsId = $("#survey_specialists___name___professionals");
const specialists = $(".specialists");
const specialistsValue = $("select#survey_specialists___name___professionals");

/* aids */
const aidsId = [
  $("#survey_aids___name___publicAssistanceFinancialSupport"),
  $("#survey_aids___name___publicDevicesToHelpPeopleStayAtHome"),
  $("#survey_aids___name___publicPlacementDevices"),
  $("#survey_aids___name___privateSystem"),
  $("#survey_aids___name___ONGFondation"),
];
const aidsValue = [
  $("select#survey_aids___name___publicAssistanceFinancialSupport"),
  $("select#survey_aids___name___publicDevicesToHelpPeopleStayAtHome"),
  $("select#survey_aids___name___publicPlacementDevices"),
  $("select#survey_aids___name___privateSystem"),
  $("select#survey_aids___name___ONGFondation"),
];

/* clothes */
const clothesId = $("#survey_clothes___name___market");

const clothes = $(".clothes");

const ClothesValue = $("select#survey_clothes___name___market");

function showInput(id, input, value) {
  id.on("change", () => {
    if (value.val() === 0) {
      input.removeClass("d-none");
      input.addClass("mb-3");
    } else {
      input.addClass("d-none");
    }
  });
}

showInput(specialistsId, specialists, specialistsValue);
for (let index = 0; index < aidsId.length; index += 1) {
  showInput(aidsId[index], $(`.aids${index}`), aidsValue[index]);
}
showInput(clothesId, clothes, ClothesValue);

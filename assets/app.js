// global script
import "bootstrap/dist/js/bootstrap";
import "./bootstrap";
import "@fortawesome/fontawesome-free/css/all.css";
import "@fortawesome/fontawesome-free/js/all";
import "./scripts/aos";
import "./scripts/parallax";
import "./scripts/floatButton";
import "./notyf";
import "./styles/app.scss";



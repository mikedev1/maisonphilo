<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230713174604 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE clinique DROP slug');
        $this->addSql('DROP INDEX IDX_4282C85BF92F3E70 ON hospital');
        $this->addSql('ALTER TABLE hospital DROP country_id, CHANGE is_not_geriatric is_geriatric TINYINT(1) DEFAULT NULL');
        $this->addSql('ALTER TABLE hospital ADD CONSTRAINT FK_4282C85B337C6FC3 FOREIGN KEY (service_platform_id) REFERENCES service_platform (id)');
        $this->addSql('ALTER TABLE hospital ADD CONSTRAINT FK_4282C85B8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE hospital ADD CONSTRAINT FK_4282C85BDF1E57AB FOREIGN KEY (quartier_id) REFERENCES quartier (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE clinique ADD slug VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE hospital DROP FOREIGN KEY FK_4282C85B337C6FC3');
        $this->addSql('ALTER TABLE hospital DROP FOREIGN KEY FK_4282C85B8BAC62AF');
        $this->addSql('ALTER TABLE hospital DROP FOREIGN KEY FK_4282C85BDF1E57AB');
        $this->addSql('ALTER TABLE hospital ADD country_id INT NOT NULL, CHANGE is_geriatric is_not_geriatric TINYINT(1) DEFAULT NULL');
        $this->addSql('CREATE INDEX IDX_4282C85BF92F3E70 ON hospital (country_id)');
    }
}

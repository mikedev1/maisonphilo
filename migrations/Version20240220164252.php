<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240220164252 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE category (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE city (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, quartier_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, slug VARCHAR(255) DEFAULT NULL, INDEX IDX_2D5B0234F92F3E70 (country_id), INDEX IDX_2D5B0234DF1E57AB (quartier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE clinique (id INT AUTO_INCREMENT NOT NULL, city_id INT NOT NULL, country_id INT DEFAULT NULL, quartier_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, phone_number VARCHAR(255) DEFAULT NULL, information VARCHAR(255) DEFAULT NULL, INDEX IDX_2246B948BAC62AF (city_id), INDEX IDX_2246B94F92F3E70 (country_id), INDEX IDX_2246B94DF1E57AB (quartier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commerces (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, category_id INT DEFAULT NULL, service_platform_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, INDEX IDX_DFD4704EF92F3E70 (country_id), INDEX IDX_DFD4704E12469DE2 (category_id), INDEX IDX_DFD4704E337C6FC3 (service_platform_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact (id INT AUTO_INCREMENT NOT NULL, last_name VARCHAR(255) DEFAULT NULL, first_name VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, email VARCHAR(255) NOT NULL, created_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', subject VARCHAR(255) NOT NULL, city VARCHAR(255) DEFAULT NULL, file VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE contact_country (contact_id INT NOT NULL, country_id INT NOT NULL, INDEX IDX_C95CE8E7E7A1254A (contact_id), INDEX IDX_C95CE8E7F92F3E70 (country_id), PRIMARY KEY(contact_id, country_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE country (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, is_valid TINYINT(1) NOT NULL, slug VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE general_terms (id INT AUTO_INCREMENT NOT NULL, legal_notice LONGTEXT DEFAULT NULL, cookie_policy LONGTEXT DEFAULT NULL, protection_of_privacy LONGTEXT DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE global_search (id INT AUTO_INCREMENT NOT NULL, path_id INT NOT NULL, link VARCHAR(255) NOT NULL, INDEX IDX_5B61CEF7D96C566B (path_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hospital (id INT AUTO_INCREMENT NOT NULL, service_platform_id INT DEFAULT NULL, city_id INT DEFAULT NULL, quartier_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, sub_title VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, is_valid TINYINT(1) DEFAULT NULL, information LONGTEXT DEFAULT NULL, is_geriatric TINYINT(1) DEFAULT NULL, INDEX IDX_4282C85B337C6FC3 (service_platform_id), INDEX IDX_4282C85B8BAC62AF (city_id), INDEX IDX_4282C85BDF1E57AB (quartier_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE partner (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, service_platform_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, address LONGTEXT DEFAULT NULL, description LONGTEXT DEFAULT NULL, type LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', activity LONGTEXT DEFAULT NULL, contact VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(255) DEFAULT NULL, email VARCHAR(255) DEFAULT NULL, is_valid TINYINT(1) DEFAULT NULL, is_honored TINYINT(1) DEFAULT NULL, image_name VARCHAR(255) DEFAULT NULL, INDEX IDX_312B3E16F92F3E70 (country_id), INDEX IDX_312B3E16337C6FC3 (service_platform_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE pharmacy (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, service_platform_id INT DEFAULT NULL, name VARCHAR(255) DEFAULT NULL, content LONGTEXT DEFAULT NULL, file_name VARCHAR(255) DEFAULT NULL, INDEX IDX_D6C15C1EF92F3E70 (country_id), INDEX IDX_D6C15C1E337C6FC3 (service_platform_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, price DOUBLE PRECISION NOT NULL, description LONGTEXT DEFAULT NULL, sub_title VARCHAR(255) DEFAULT NULL, image VARCHAR(255) DEFAULT NULL, updated_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_D34A04ADF92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product_category (product_id INT NOT NULL, category_id INT NOT NULL, INDEX IDX_CDFC73564584665A (product_id), INDEX IDX_CDFC735612469DE2 (category_id), PRIMARY KEY(product_id, category_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE quartier (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE reset_password_request (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, selector VARCHAR(20) NOT NULL, hashed_token VARCHAR(100) NOT NULL, requested_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', expires_at DATETIME NOT NULL COMMENT \'(DC2Type:datetime_immutable)\', INDEX IDX_7CE748AA76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE route (id INT AUTO_INCREMENT NOT NULL, path VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE service_platform (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, path VARCHAR(255) DEFAULT NULL, is_valid TINYINT(1) NOT NULL, order_list SMALLINT DEFAULT NULL, is_not_working TINYINT(1) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE site_internet (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, service_platform_id INT DEFAULT NULL, description LONGTEXT NOT NULL, INDEX IDX_70BEFF8BF92F3E70 (country_id), INDEX IDX_70BEFF8B337C6FC3 (service_platform_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE social_center (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, service_platform_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, INDEX IDX_5EBB89F8F92F3E70 (country_id), INDEX IDX_5EBB89F8337C6FC3 (service_platform_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE state_device (id INT AUTO_INCREMENT NOT NULL, country_id INT NOT NULL, service_platform_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, description LONGTEXT NOT NULL, links LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_EB6921F92F3E70 (country_id), INDEX IDX_EB6921337C6FC3 (service_platform_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE survey (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) DEFAULT NULL, last_name VARCHAR(255) DEFAULT NULL, email VARCHAR(255) NOT NULL, created_at DATETIME DEFAULT NULL COMMENT \'(DC2Type:datetime_immutable)\', country VARCHAR(255) DEFAULT NULL, market VARCHAR(255) DEFAULT NULL, clothes LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', specialists LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', hospitals LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', aids LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', digits LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', politics LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE telephone (id INT AUTO_INCREMENT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, country_id INT DEFAULT NULL, email VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, is_verified TINYINT(1) NOT NULL, nickname VARCHAR(50) DEFAULT NULL, first_name VARCHAR(50) DEFAULT NULL, last_name VARCHAR(50) DEFAULT NULL, image_name VARCHAR(255) DEFAULT NULL, phone_number VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), INDEX IDX_8D93D649F92F3E70 (country_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE city ADD CONSTRAINT FK_2D5B0234DF1E57AB FOREIGN KEY (quartier_id) REFERENCES quartier (id)');
        $this->addSql('ALTER TABLE clinique ADD CONSTRAINT FK_2246B948BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE clinique ADD CONSTRAINT FK_2246B94F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE clinique ADD CONSTRAINT FK_2246B94DF1E57AB FOREIGN KEY (quartier_id) REFERENCES quartier (id)');
        $this->addSql('ALTER TABLE commerces ADD CONSTRAINT FK_DFD4704EF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE commerces ADD CONSTRAINT FK_DFD4704E12469DE2 FOREIGN KEY (category_id) REFERENCES route (id)');
        $this->addSql('ALTER TABLE commerces ADD CONSTRAINT FK_DFD4704E337C6FC3 FOREIGN KEY (service_platform_id) REFERENCES service_platform (id)');
        $this->addSql('ALTER TABLE contact_country ADD CONSTRAINT FK_C95CE8E7E7A1254A FOREIGN KEY (contact_id) REFERENCES contact (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE contact_country ADD CONSTRAINT FK_C95CE8E7F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE global_search ADD CONSTRAINT FK_5B61CEF7D96C566B FOREIGN KEY (path_id) REFERENCES route (id)');
        $this->addSql('ALTER TABLE hospital ADD CONSTRAINT FK_4282C85B337C6FC3 FOREIGN KEY (service_platform_id) REFERENCES service_platform (id)');
        $this->addSql('ALTER TABLE hospital ADD CONSTRAINT FK_4282C85B8BAC62AF FOREIGN KEY (city_id) REFERENCES city (id)');
        $this->addSql('ALTER TABLE hospital ADD CONSTRAINT FK_4282C85BDF1E57AB FOREIGN KEY (quartier_id) REFERENCES quartier (id)');
        $this->addSql('ALTER TABLE partner ADD CONSTRAINT FK_312B3E16F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE partner ADD CONSTRAINT FK_312B3E16337C6FC3 FOREIGN KEY (service_platform_id) REFERENCES service_platform (id)');
        $this->addSql('ALTER TABLE pharmacy ADD CONSTRAINT FK_D6C15C1EF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE pharmacy ADD CONSTRAINT FK_D6C15C1E337C6FC3 FOREIGN KEY (service_platform_id) REFERENCES service_platform (id)');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC73564584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE product_category ADD CONSTRAINT FK_CDFC735612469DE2 FOREIGN KEY (category_id) REFERENCES category (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE reset_password_request ADD CONSTRAINT FK_7CE748AA76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE site_internet ADD CONSTRAINT FK_70BEFF8BF92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE site_internet ADD CONSTRAINT FK_70BEFF8B337C6FC3 FOREIGN KEY (service_platform_id) REFERENCES service_platform (id)');
        $this->addSql('ALTER TABLE social_center ADD CONSTRAINT FK_5EBB89F8F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE social_center ADD CONSTRAINT FK_5EBB89F8337C6FC3 FOREIGN KEY (service_platform_id) REFERENCES service_platform (id)');
        $this->addSql('ALTER TABLE state_device ADD CONSTRAINT FK_EB6921F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
        $this->addSql('ALTER TABLE state_device ADD CONSTRAINT FK_EB6921337C6FC3 FOREIGN KEY (service_platform_id) REFERENCES service_platform (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649F92F3E70 FOREIGN KEY (country_id) REFERENCES country (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE city DROP FOREIGN KEY FK_2D5B0234F92F3E70');
        $this->addSql('ALTER TABLE city DROP FOREIGN KEY FK_2D5B0234DF1E57AB');
        $this->addSql('ALTER TABLE clinique DROP FOREIGN KEY FK_2246B948BAC62AF');
        $this->addSql('ALTER TABLE clinique DROP FOREIGN KEY FK_2246B94F92F3E70');
        $this->addSql('ALTER TABLE clinique DROP FOREIGN KEY FK_2246B94DF1E57AB');
        $this->addSql('ALTER TABLE commerces DROP FOREIGN KEY FK_DFD4704EF92F3E70');
        $this->addSql('ALTER TABLE commerces DROP FOREIGN KEY FK_DFD4704E12469DE2');
        $this->addSql('ALTER TABLE commerces DROP FOREIGN KEY FK_DFD4704E337C6FC3');
        $this->addSql('ALTER TABLE contact_country DROP FOREIGN KEY FK_C95CE8E7E7A1254A');
        $this->addSql('ALTER TABLE contact_country DROP FOREIGN KEY FK_C95CE8E7F92F3E70');
        $this->addSql('ALTER TABLE global_search DROP FOREIGN KEY FK_5B61CEF7D96C566B');
        $this->addSql('ALTER TABLE hospital DROP FOREIGN KEY FK_4282C85B337C6FC3');
        $this->addSql('ALTER TABLE hospital DROP FOREIGN KEY FK_4282C85B8BAC62AF');
        $this->addSql('ALTER TABLE hospital DROP FOREIGN KEY FK_4282C85BDF1E57AB');
        $this->addSql('ALTER TABLE partner DROP FOREIGN KEY FK_312B3E16F92F3E70');
        $this->addSql('ALTER TABLE partner DROP FOREIGN KEY FK_312B3E16337C6FC3');
        $this->addSql('ALTER TABLE pharmacy DROP FOREIGN KEY FK_D6C15C1EF92F3E70');
        $this->addSql('ALTER TABLE pharmacy DROP FOREIGN KEY FK_D6C15C1E337C6FC3');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADF92F3E70');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC73564584665A');
        $this->addSql('ALTER TABLE product_category DROP FOREIGN KEY FK_CDFC735612469DE2');
        $this->addSql('ALTER TABLE reset_password_request DROP FOREIGN KEY FK_7CE748AA76ED395');
        $this->addSql('ALTER TABLE site_internet DROP FOREIGN KEY FK_70BEFF8BF92F3E70');
        $this->addSql('ALTER TABLE site_internet DROP FOREIGN KEY FK_70BEFF8B337C6FC3');
        $this->addSql('ALTER TABLE social_center DROP FOREIGN KEY FK_5EBB89F8F92F3E70');
        $this->addSql('ALTER TABLE social_center DROP FOREIGN KEY FK_5EBB89F8337C6FC3');
        $this->addSql('ALTER TABLE state_device DROP FOREIGN KEY FK_EB6921F92F3E70');
        $this->addSql('ALTER TABLE state_device DROP FOREIGN KEY FK_EB6921337C6FC3');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649F92F3E70');
        $this->addSql('DROP TABLE category');
        $this->addSql('DROP TABLE city');
        $this->addSql('DROP TABLE clinique');
        $this->addSql('DROP TABLE commerces');
        $this->addSql('DROP TABLE contact');
        $this->addSql('DROP TABLE contact_country');
        $this->addSql('DROP TABLE country');
        $this->addSql('DROP TABLE general_terms');
        $this->addSql('DROP TABLE global_search');
        $this->addSql('DROP TABLE hospital');
        $this->addSql('DROP TABLE partner');
        $this->addSql('DROP TABLE pharmacy');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE product_category');
        $this->addSql('DROP TABLE quartier');
        $this->addSql('DROP TABLE reset_password_request');
        $this->addSql('DROP TABLE route');
        $this->addSql('DROP TABLE service_platform');
        $this->addSql('DROP TABLE site_internet');
        $this->addSql('DROP TABLE social_center');
        $this->addSql('DROP TABLE state_device');
        $this->addSql('DROP TABLE survey');
        $this->addSql('DROP TABLE telephone');
        $this->addSql('DROP TABLE user');
    }
}
